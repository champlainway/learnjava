package net.smallbulletin.recursionapp.util;

import static org.junit.Assert.*;

import org.junit.Test;

public class TriangleStackTest {

    @Test
    public void testGetNums() {
        TriangleStack nums = new TriangleStack(10);

        assertEquals(55, nums.getNums());

        nums = new TriangleStack(0);

        assertEquals(0, nums.getNums());
    }

}
