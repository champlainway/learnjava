package net.smallbulletin.recursionapp.util;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class KnapsackTest {

    private final List<Integer> items = new ArrayList<Integer>();

    @Before
    public void before() {
        items.add(11);
        items.add(8);
        items.add(7);
        items.add(6);
        items.add(5);
    }

    @Test
    public void testOne() {

        final int target = 20;

        final List<Integer> nums = Knapsack.run(items, target);

        final List<Integer> expected = new ArrayList<Integer>();

        expected.add(5);
        expected.add(7);
        expected.add(8);

        for (int i = 0; i < nums.size(); i++) {
            assertEquals(expected.get(i), nums.get(i));
        }

    }

    @Test
    public void testTwo() {
        final List<Integer> nums = Knapsack.run(items, 1);
        assertNull(nums);

    }

    @Test
    public void testThree() {
        final int target = 17;

        final List<Integer> nums = Knapsack.run(items, target);

        final List<Integer> expected = new ArrayList<Integer>();

        expected.add(6);
        expected.add(11);

        for (int i = 0; i < nums.size(); i++) {
            assertEquals(expected.get(i), nums.get(i));
        }
    }

    @Test
    public void testFour() {
        final int target = 5;

        final List<Integer> nums = Knapsack.run(items, target);

        final List<Integer> expected = new ArrayList<Integer>();

        expected.add(5);

        for (int i = 0; i < expected.size(); i++) {
            assertEquals(expected.get(i), nums.get(i));
        }
    }

}
