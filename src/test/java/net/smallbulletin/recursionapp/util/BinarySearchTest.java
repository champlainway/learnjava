package net.smallbulletin.recursionapp.util;

import static org.junit.Assert.*;

import org.junit.Test;

public class BinarySearchTest {

    @Test
    public void testSearch() {
        String[] array = { "a", "b", "c", "d", "e" };

        int index = BinarySearch.search(array, "b", 0, array.length - 1);

        assertEquals(index, 1);
        assertEquals(-1, BinarySearch.search(array, "f", 0, array.length - 1));
        array = new String[0];
        assertEquals(-1, BinarySearch.search(array, "a", 0, array.length - 1));
        array = new String[1];
        array[0] = "a";
        assertEquals(0, BinarySearch.search(array, "a", 0, array.length - 1));
        array = new String[2];
        array[0] = "a";
        array[1] = "b";

        assertEquals(1, BinarySearch.search(array, "b", 0, array.length - 1));
    }

}
