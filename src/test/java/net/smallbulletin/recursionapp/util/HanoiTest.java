package net.smallbulletin.recursionapp.util;

import java.util.List;

import org.junit.Test;

public class HanoiTest {

    @Test
    public void testHanoi() {

        final Hanoi hanoi = new Hanoi(3);

        hanoi.run();
        final List<String> list = hanoi.getPics();
        for (final String s : list) {
            System.out.print(s);
        }
    }

}
