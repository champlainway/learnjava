package net.smallbulletin.recursionapp.util;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import jersey.repackaged.com.google.common.collect.Lists;

public class SelectTeamTest {

    private final List<String> members = new ArrayList<>();

    @Before
    public void before() {
        members.add("A");
        members.add("B");
        members.add("C");
        members.add("D");
        members.add("E");
    }

    @Test
    public void testRun() {
        final int numOfPeople = 3;

        final List<String> answer = SelectTeam.run(members, numOfPeople);

        final List<String> real = Lists.newArrayList();

        real.add("ABC");
        real.add("ABD");
        real.add("ABE");
        real.add("ACD");
        real.add("ACE");
        real.add("ADE");
        real.add("BCD");
        real.add("BCE");
        real.add("BDE");
        real.add("CDE");

        for (int i = 0; i < answer.size(); i++) {
            assertEquals(real.get(i), answer.get(i));
        }
        assertEquals(real.size(), answer.size());
    }

}
