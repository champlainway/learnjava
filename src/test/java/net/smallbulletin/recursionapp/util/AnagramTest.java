package net.smallbulletin.recursionapp.util;

import static org.junit.Assert.*;

import java.util.Set;

import org.junit.Test;

import com.google.common.collect.Sets;

public class AnagramTest {

    @Test
    public void testAnagram() {

        final Set<String> actual = Anagram.anagram("cat");

        assertEquals(6, actual.size());

        final Set<String> expected = Sets.newHashSet("cat", "cta", "act", "atc", "tac", "tca");

        assertEquals(expected, actual);
    }

    @Test
    public void testAnagramEmpty() {

        final Set<String> actual = Anagram.anagram("");

        assertEquals(1, actual.size());

        final Set<String> expected = Sets.newHashSet("");

        assertEquals(expected, actual);
    }
}
