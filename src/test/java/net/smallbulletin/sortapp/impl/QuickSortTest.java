package net.smallbulletin.sortapp.impl;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import net.smallbulletin.sortapp.Output;

public class QuickSortTest {

    private final List<String> list = new ArrayList<>();

    @Before
    public void before() {
        list.add("C");
        list.add("E");
        list.add("Z");
        list.add("A");
        list.add("F");
        list.add("X");
        list.add("V");
        list.add("M");
        list.add("B");

    }

    @Test
    public void testSort() {
        final QuickSort sort = new QuickSort();

        final Output<String> out = sort.sort(list);

        final List<String> expected = new ArrayList<>();

        expected.add("A");
        expected.add("B");
        expected.add("C");
        expected.add("E");
        expected.add("F");
        expected.add("M");
        expected.add("V");
        expected.add("X");
        expected.add("Z");

        for (int i = 0; i < expected.size(); i++) {
            assertEquals(expected.get(i), out.getSorted().get(i));
        }

        assertEquals(expected.size(), out.getSorted().size());
    }

    @Test
    public void testSortTwo() {
        final List<String> list = new ArrayList<>();

        list.add("C");
        list.add("A");
        list.add("B");

        final QuickSort sort = new QuickSort();

        final Output<String> out = sort.sort(list);

        final List<String> expected = new ArrayList<>();

        expected.add("A");
        expected.add("B");
        expected.add("C");

        for (int i = 0; i < list.size(); i++) {
            assertEquals(expected.get(i), out.getSorted().get(i));
        }

        assertEquals(expected.size(), out.getSorted().size());
    }

}
