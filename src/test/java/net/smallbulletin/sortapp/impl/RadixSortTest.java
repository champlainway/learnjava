package net.smallbulletin.sortapp.impl;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;

import net.smallbulletin.sortapp.Output;

public class RadixSortTest {

    private List<Integer> list;

    @Before
    public void before() {
        list = ImmutableList.of(5, 1, 7, 2, 8, 11, 6, 12, 9, 20);
    }

    @Test
    public void testSort() {
        final RadixSort sort = new RadixSort();
        final Output<Integer> output = sort.sort(list);
        final List<Integer> expected = ImmutableList.of(1, 2, 5, 6, 7, 8, 9, 11, 12, 20);
        assertEquals(expected.size(), output.getSorted().size());
        for (int i = 0; i < expected.size(); i++) {
            assertEquals(expected.get(i), output.getSorted().get(i));
        }
    }

}
