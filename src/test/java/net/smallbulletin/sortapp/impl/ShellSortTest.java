package net.smallbulletin.sortapp.impl;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import net.smallbulletin.sortapp.Output;

public class ShellSortTest {
    private final List<Integer> input = new ArrayList<>();

    // [5, 3, 6, 1, 7, 4, 15, 11, 25, 21, 2, 22, 8, 23, 10]
    @Before
    public void before() {
        input.add(5);
        input.add(3);
        input.add(6);
        input.add(1);
        input.add(7);
        input.add(4);
        input.add(15);
        input.add(11);
        input.add(25);
        input.add(21);
        input.add(2);
        input.add(22);
        input.add(8);
        input.add(23);
        input.add(10);
        input.add(9);
        input.add(12);
        input.add(24);
        input.add(13);
        input.add(20);
        input.add(14);
        input.add(19);
        input.add(16);
        input.add(18);
        input.add(17);

    }

    @Test
    public void testSort() {
        final ShellSort obj = new ShellSort();

        final Output<Integer> out = obj.sort(input);
        final List<Integer> sorted = out.getSorted();

        for (int i = 1; i <= 25; i++) {
            assertEquals((Integer) i, sorted.get(i - 1));
        }
        assertEquals(25, sorted.size());
    }

}
