package net.smallbulletin.huffman.util;

import static org.junit.Assert.*;

import org.junit.Test;

public class HuffmanTreeTest {

    private HuffmanEncryption testTree;
    private String str = "THIS"; // "SUSIE SAYS IT IS EASY";

    @Test
    public void testSerialize() {
        testTree = new HuffmanEncryption(str);
        final String serialized = testTree.getDecodeTree().serialize();
        assertEquals("((((^^H)(^^T)#)((^^S)(^^I)#)#)^#)", serialized);
    }

    @Test
    public void testDeserialize() {
        final HuffmanTree test = HuffmanTree.deserialize("((((^^H)(^^T)#)((^^S)(^^I)#)#)^#)");

        assertEquals("((((^^H)(^^T)#)((^^S)(^^I)#)#)^#)", test.serialize());
    }

    @Test
    public void testSerializeTwo() {
        str = "";
        testTree = new HuffmanEncryption(str);
        final String serialized = testTree.getDecodeTree().serialize();

        assertEquals("(^^#)", serialized);
    }

    @Test
    public void testDeserializeTwo() {
        final HuffmanTree test = HuffmanTree.deserialize("(^^#)");

        assertEquals("(^^#)", test.serialize());
    }

    @Test
    public void testSerializeThree() {
        str = "A";
        testTree = new HuffmanEncryption(str);
        final String serialized = testTree.getDecodeTree().serialize();

        assertEquals("(^^A)", serialized);
    }

    @Test
    public void testDeserializeThree() {
        final HuffmanTree test = HuffmanTree.deserialize("(^^A)");

        assertEquals("(^^A)", test.serialize());
    }

    @Test
    public void testSerializeFour() {
        str = "AAAAABBBBAACCCCCCAABC";
        testTree = new HuffmanEncryption(str);
        final String serialized = testTree.getDecodeTree().serialize();

        assertEquals("(((^^A)((^^B)(^^C)#)#)^#)", serialized);
    }

    @Test
    public void testDeserializeFour() {
        final HuffmanTree test = HuffmanTree.deserialize("(((^^A)((^^B)(^^C)#)#)^#)");

        assertEquals("(((^^A)((^^B)(^^C)#)#)^#)", test.serialize());
    }

}
