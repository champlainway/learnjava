package net.smallbulletin.listapp.util;

import static org.junit.Assert.*;

import org.junit.Test;

public class LinkTest {

    @Test
    public void testLink() {
        final String value = "abc";
        Link<String> test = new Link<>(value);
        assertNotNull(test);
        assertNull(test.getNext());

        assertSame(value, test.getValue());
    }

    @Test
    public void testLinkELinkOfELinkOfE() {
        final String value = "abc";
        Link<String> next = new Link<>("next");

        Link<String> test = new Link<>(value, next);
        assertNotNull(test);
        assertSame(next, test.getNext());

        assertSame(value, test.getValue());
    }

    @Test
    public void testSetNext() {
        final String value = "abc";
        Link<String> test = new Link<>(value);
        assertNotNull(test);
        assertNull(test.getNext());

        assertSame(value, test.getValue());

        Link<String> next = new Link<>("next");
        test.setNext(next);
        assertSame(next, test.getNext());

    }

}
