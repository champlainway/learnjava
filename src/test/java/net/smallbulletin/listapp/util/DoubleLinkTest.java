package net.smallbulletin.listapp.util;

import static org.junit.Assert.*;

import org.junit.Test;

public class DoubleLinkTest {

    @Test
    public void testDoubleLinkE() {
        DoubleLink<String> link = new DoubleLink<>("1");

        assertEquals("1", link.getValue());
        assertNull(link.getPrev());
        assertNull(link.getNext());

    }

    @Test
    public void testDoubleLinkEDoubleLinkOfEDoubleLinkOfE() {
        DoubleLink<String> prev = new DoubleLink<>("1");
        DoubleLink<String> next = new DoubleLink<>("2");
        DoubleLink<String> link = new DoubleLink<>("3", prev, next);

        assertSame("3", link.getValue());
        assertSame(prev, link.getPrev());
        assertSame(next, link.getNext());
        assertSame(link, next.getPrev());
        assertSame(link, prev.getNext());
    }

    @Test
    public void testGetNext() {
        DoubleLink<String> next = new DoubleLink<>("1");
        DoubleLink<String> link = new DoubleLink<>("2");

        link.setNext(next);

        assertSame("2", link.getValue());
        assertSame(next, link.getNext());
        assertNull(link.getPrev());
        assertNull(next.getNext());
    }

    @Test
    public void testSetNext() {
        DoubleLink<String> next = new DoubleLink<>("1");
        DoubleLink<String> link = new DoubleLink<>("2");

        assertSame("2", link.getValue());
        assertNull(link.getNext());
        link.setNext(next);

        assertSame(next, link.getNext());
        assertNull(link.getPrev());
        assertNull(next.getNext());
    }

    @Test
    public void testGetPrev() {
        DoubleLink<String> prev = new DoubleLink<>("1");
        DoubleLink<String> link = new DoubleLink<>("2");

        link.setPrev(prev);

        assertSame("2", link.getValue());
        assertSame(prev, link.getPrev());
        assertNull(link.getNext());
        assertNull(prev.getPrev());
    }

    @Test
    public void testSetPrev() {
        DoubleLink<String> prev = new DoubleLink<>("1");
        DoubleLink<String> link = new DoubleLink<>("2");

        assertSame("2", link.getValue());
        assertNull(link.getPrev());
        link.setPrev(prev);

        assertSame(prev, link.getPrev());
        assertNull(link.getNext());
        assertNull(prev.getPrev());
    }

    @Test
    public void testGetValue() {
        DoubleLink<String> link = new DoubleLink<>("1");

        assertSame("1", link.getValue());
    }

}
