package net.smallbulletin.listapp.util;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

public class CircularLinkedListTest {
    CircularLinkedList<String> list = new CircularLinkedList<>();

    @Test
    public void testCircularLinkedList() {

        final List<String> data = Arrays.asList("1", "2", "3", "4", "5");
        this.list = new CircularLinkedList<>(data);
        assertEquals("[1, 2, 3, 4, 5]", this.list.toString());
        assertEquals(5, this.list.size());
    }

    @Test
    public void testAdd() {

        assertEquals("[]", this.list.toString());
        assertEquals(0, this.list.size());

        this.list.add("1");
        assertEquals("[1]", this.list.toString());
        assertEquals(1, this.list.size());

        this.list.add("2");
        assertEquals("[1, 2]", this.list.toString());
        assertEquals(2, this.list.size());

        this.list.add("3");
        assertEquals("[1, 2, 3]", this.list.toString());
        assertEquals(3, this.list.size());
    }

    @Test
    public void testRemoveCurr() {

        this.list.add("1");
        this.list.add("2");
        this.list.add("3");
        assertEquals("[1, 2, 3]", this.list.toString());
        assertEquals(3, this.list.size());

        this.list.removeCurr();
        assertEquals("[2, 3]", this.list.toString());
        assertEquals(2, this.list.size());

        this.list.removeCurr();
        assertEquals("[3]", this.list.toString());
        assertEquals(1, this.list.size());

        this.list.removeCurr();
        assertEquals("[]", this.list.toString());
        assertEquals(0, this.list.size());
    }

    @Test
    public void testGetCurr() {

        this.list.add("1");
        this.list.add("2");
        this.list.add("3");

        assertEquals("1", this.list.getCurr());
        assertEquals("1", this.list.getCurr());
        this.list.next();
        assertEquals("2", this.list.getCurr());
        this.list.next();
        assertEquals("3", this.list.getCurr());
        this.list.next();
        assertEquals("1", this.list.getCurr());
    }

    @Test
    public void testNext() {

        this.list.add("1");
        this.list.add("2");
        this.list.add("3");

        assertEquals("1", this.list.next());

        assertEquals("2", this.list.next());

        assertEquals("3", this.list.next());

        assertEquals("1", this.list.next());
    }

    @Test
    public void testSize() {
        this.list.add("1");
        this.list.add("2");
        this.list.add("3");
        assertEquals(3, this.list.size());
        this.list.removeCurr();
        assertEquals(2, this.list.size());
        this.list.add("3");
        this.list.add("4");
        assertEquals(4, this.list.size());

    }

    @Test
    public void testToString() {

        this.list.add("1");
        this.list.add("2");
        this.list.add("3");
        assertEquals("[1, 2, 3]", this.list.toString());

    }

}
