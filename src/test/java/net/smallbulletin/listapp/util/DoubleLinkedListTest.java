package net.smallbulletin.listapp.util;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;

import org.junit.Test;

public class DoubleLinkedListTest {
	
	private List<String> list = new DoubleLinkedList<>();
	
	@Test
	public void testAddE() {
		assertTrue(list.isEmpty());
		list.add("a");
		assertEquals("a", list.get(0));
		list.add("b");
		assertEquals("b", list.get(1));
		list.add("c");
		assertEquals("c", list.get(2));
		list.add("d");
		assertEquals("d", list.get(3));
	}

	@Test
	public void testAddIntE() {
		assertTrue(list.isEmpty());
		list.add(0, "a");
		assertEquals("a", list.get(0));
		list.add(1, "b");
		assertEquals("b", list.get(1));
		list.add(0, "c");
		assertEquals("c", list.get(0));
		assertEquals("a", list.get(1));
		assertEquals("b", list.get(2));

		try {
			list.add(100, "fail");
			fail("Index is out of bounds");
		} catch (IndexOutOfBoundsException e) {

		}
	}

	@Test
	public void testAddAllCollectionOfQextendsE() {
		ArrayList<String> toAdd = new ArrayList<>();
		toAdd.add("a");
		toAdd.add("b");
		toAdd.add("c");
		toAdd.add("d");
		toAdd.add("e");
		toAdd.add("f");
		toAdd.add("g");

		list.addAll(toAdd);

		for (int i = 0; i < list.size(); i++) {
			assertEquals(toAdd.get(i), list.get(i));
		}

		assertEquals(toAdd.size(), list.size());
	}

	@Test
	public void testAddAllIntCollectionOfQextendsE() {
		list.add("h");
		list.add("i");
		list.add("j");

		ArrayList<String> toAdd = new ArrayList<>();
		toAdd.add("a");
		toAdd.add("b");
		toAdd.add("c");
		toAdd.add("d");
		toAdd.add("e");
		toAdd.add("f");
		toAdd.add("g");

		list.addAll(1, toAdd);

		for (int i = 0; i < toAdd.size(); i++) {
			assertEquals(toAdd.get(i), list.get(i + 1));
		}

		assertEquals("h", list.get(0));
		assertEquals("i", list.get(8));
		assertEquals("j", list.get(9));
	}

	@Test
	public void testClear() {
		list.add("a");
		list.add("b");
		list.add("c");
		list.add("d");

		assertFalse(list.isEmpty());
		assertEquals(4, list.size());
		list.clear();
		assertTrue(list.isEmpty());
		assertEquals(0, list.size());

	}

	@Test
	public void testContains() {
		assertFalse(list.contains("1"));

		list.add("a");
		list.add("b");
		list.add("c");

		assertTrue(list.contains("a"));
		assertTrue(list.contains("b"));
		assertTrue(list.contains("c"));

		assertFalse(list.contains("1"));
	}

	@Test
	public void testContainsAll() {
		list.add("a");
		list.add("b");
		list.add("c");

		ArrayList<String> toCheck = new ArrayList<>();
		toCheck.add("a");
		toCheck.add("b");
		toCheck.add("c");
		toCheck.add("d");

		assertFalse(list.containsAll(toCheck));

		toCheck.remove("d");

		assertTrue(list.containsAll(toCheck));
	}

	@Test
	public void testGet() {
		assertTrue(list.isEmpty());
		list.add("a");
		assertEquals("a", list.get(0));
		list.add("b");
		assertEquals("b", list.get(1));
		list.add("c");
		assertEquals("c", list.get(2));
		list.add("d");
		assertEquals("d", list.get(3));

		try {
			list.get(100);

			fail("Index out of bounds");
		} catch (IndexOutOfBoundsException e) {

		}
	}

	@Test
	public void testIndexOf() {
		list.add("a");
		list.add("b");
		list.add("c");

		assertEquals(0, list.indexOf("a"));
		assertEquals(1, list.indexOf("b"));
		assertEquals(2, list.indexOf("c"));

		assertEquals(-1, list.indexOf("d"));
	}

	@Test
	public void testIsEmpty() {
		assertTrue(list.isEmpty());
		list.add("a");
		assertFalse(list.isEmpty());
		list.remove("a");
		assertTrue(list.isEmpty());
	}

	@Test
	public void testIterator() {
		list.add("a");
		list.add("b");
		list.add("c");
		list.add("d");

		Iterator<String> iter = list.iterator();

		assertEquals("a", iter.next());
		assertEquals("b", iter.next());
		assertEquals("c", iter.next());
		assertTrue(iter.hasNext());
		assertEquals("d", iter.next());
		assertFalse(iter.hasNext());

		try {
			iter.next();
			fail("There should not be an element here");
		} catch (NoSuchElementException e) {

		}
	}

	@Test
	public void testLastIndexOf() {
		list.add("a");
		list.add("b");
		list.add("c");
		list.add("c");
		list.add("b");
		list.add("a");

		assertEquals(5, list.lastIndexOf("a"));
		assertEquals(4, list.lastIndexOf("b"));
		assertEquals(3, list.lastIndexOf("c"));

	}

	@Test
	public void testListIterator() {
		list.add("1");
		list.add("2");
		list.add("3");
		list.add("4");
		list.add("5");

		ListIterator<String> iter = list.listIterator();
		try {
			iter.previous();
			fail("No such element");
		} catch (NoSuchElementException e) {

		}
		assertFalse(iter.hasPrevious());
		assertEquals("1", iter.next());
		assertEquals("2", iter.next());
		assertEquals("2", iter.previous());
		iter.remove();
		assertFalse(list.contains("2"));
		assertEquals("3", iter.next());
		assertEquals("4", iter.next());
		assertEquals("5", iter.next());
		iter.set("6");

		assertFalse(list.contains("5"));
		assertTrue(list.contains("6"));
		assertEquals("6", iter.previous());
		iter.add("7");
		assertEquals("7", iter.previous());
		iter.add("8");
		System.out.println(list);
		assertEquals("8", iter.previous());
		assertTrue(iter.hasPrevious());
		assertEquals(3, iter.nextIndex());
		assertEquals(2, iter.previousIndex());
		
		assertEquals("8", iter.next());
		assertEquals("7", iter.next());
		assertEquals("6", iter.next());
		
		try {
			iter.next();

			fail("No such element");
		} catch (NoSuchElementException e) {

		}
	}

	@Test
	public void testListIteratorInt() {
		list.add("1");
		list.add("2");
		list.add("3");
		list.add("4");
		list.add("5");

		ListIterator<String> iter = list.listIterator(1);
		assertEquals("2", iter.next());
		assertEquals("2", iter.previous());
		iter.remove();
		assertEquals("3", iter.next());
	}

	@Test
	public void testRemoveObject() {
		list.add("a");
		list.add("b");
		list.add("c");

		assertTrue(list.remove("a"));
		assertEquals("b", list.get(0));
		assertEquals("c", list.get(1));

		assertFalse(list.contains("a"));
	}

	@Test
	public void testRemoveInt() {
		list.add("a");
		list.add("b");
		list.add("c");
		list.add("d");

		assertEquals("b", list.remove(1));
		assertFalse(list.contains("b"));
		assertEquals("a", list.get(0));
		assertEquals("c", list.get(1));
		assertEquals("d", list.get(2));
	}

	@Test
	public void testRemoveAll() {
		list.add("a");
		list.add("b");
		list.add("c");
		list.add("d");
		list.add("e");
		list.add("f");
		list.add("g");

		ArrayList<String> toRemove = new ArrayList<>();
		toRemove.add("a");
		toRemove.add("b");
		toRemove.add("c");
		toRemove.add("d");

		assertTrue(list.removeAll(toRemove));

		assertFalse(list.contains("a"));
		assertFalse(list.contains("b"));
		assertFalse(list.contains("c"));
		assertFalse(list.contains("d"));
		assertEquals("e", list.get(0));
		assertEquals("f", list.get(1));
		assertEquals("g", list.get(2));
	}

	@Test
	public void testRetainAll() {
		list.add("a");
		list.add("b");
		list.add("c");
		list.add("d");
		list.add("e");
		list.add("f");
		list.add("g");

		ArrayList<String> toRetain = new ArrayList<>();
		toRetain.add("a");
		toRetain.add("b");
		toRetain.add("c");
		toRetain.add("d");

		assertTrue(list.retainAll(toRetain));

		assertFalse(list.contains("e"));
		assertFalse(list.contains("f"));
		assertFalse(list.contains("g"));
		assertEquals("a", list.get(0));
		assertEquals("b", list.get(1));
		assertEquals("c", list.get(2));
		assertEquals("d", list.get(3));
	}

	@Test
	public void testSet() {
		list.add("a");
		list.add("b");
		list.add("c");
		list.add("d");

		list.set(1, "e");

		assertFalse(list.contains("b"));
		assertTrue(list.contains("e"));
		assertEquals("e", list.get(1));
		try {
			list.set(5, "z");

			fail("Index is out of bounds");
		} catch (IndexOutOfBoundsException e) {

		}
	}

	@Test
	public void testSize() {
		assertEquals(0, list.size());
		list.add("a");
		assertEquals(1, list.size());
		list.add("b");
		assertEquals(2, list.size());
		list.add("c");
		assertEquals(3, list.size());
		list.add("d");
		assertEquals(4, list.size());
	}

	@Test
	public void testSubList() {
		list.add("a");
		list.add("b");
		list.add("c");
		list.add("d");
		list.add("e");

		List<String> subList = list.subList(1, 3);
		int i = 1;
		for (String s : subList) {
			assertEquals(list.get(i), s);
			i++;
		}
		assertEquals(2, subList.size());
	}

	@Test
	public void testToArray() {
		list.add("a");
		list.add("b");
		list.add("c");
		list.add("d");

		Object[] array = list.toArray();

		for (int i = 0; i < list.size(); i++) {
			assertEquals(array[i], list.get(i));
		}

		assertEquals(array.length, list.size());
	}

	@Test
	public void testToArrayTArray() {
		list.add("1");
		list.add("2");
		list.add("3");
		list.add("4");
		list.add("5");

		String[] array = new String[1];

		array = list.toArray(array);

		for (int i = 0; i < list.size(); i++) {
			assertEquals(list.get(i), array[i]);
		}

		assertEquals(list.size(), array.length);

		array = new String[5];

		array = list.toArray(array);

		for (int i = 0; i < list.size(); i++) {
			assertEquals(list.get(i), array[i]);
		}

		assertEquals(list.size(), array.length);

		final String[] original6 = new String[6];
		array = list.toArray(original6);
		assertSame(original6, array);

		int length = 0;

		int i = 0;

		while (i < array.length && array[i] != null) {
			length++;
			assertEquals(list.get(i), array[i]);
			i++;
		}

		assertEquals(list.size(), length);
	}

}
