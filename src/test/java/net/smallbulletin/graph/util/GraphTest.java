package net.smallbulletin.graph.util;

import static org.junit.Assert.*;

import java.util.NoSuchElementException;

import org.junit.Before;
import org.junit.Test;

public class GraphTest {

    Graph<Integer> test = new Graph<>();

    @Before
    public void before() {
        test.addVertex(1);
        test.addVertex(2);
        test.addVertex(3);
        test.addVertex(4);
    }

    @Test
    public void testAddVertex() {
        assertEquals(4, test.getAdj().length);
        test.addVertex(5);
        assertEquals(5, test.getAdj().length);
        assertEquals(new Integer(5), test.get(4).getValue());
        assertEquals(new Integer(4), test.get(3).getValue());
        assertEquals(new Integer(3), test.get(2).getValue());
        assertEquals(new Integer(2), test.get(1).getValue());
        assertEquals(new Integer(1), test.get(0).getValue());
        test.addVertex(3);
        try {
            test.get(5);
            fail("Shouldn't have been added");
        } catch (final IndexOutOfBoundsException e) {

        }

    }

    @Test
    public void testAddEdgeValue() {
        test.addVertex(5);
        test.addEdgeValue(1, 3);
        test.addEdgeValue(3, 4);
        test.addEdgeValue(2, 5);
        test.addEdgeValue(1, 2);

        assertEquals("12534", test.dfSearch());
        assertEquals("12354", test.bfSearch());
    }

    @Test
    public void testAddEdgeValueTwo() {
        try {
            test.addEdgeValue(100, 2);
            fail("No element exists");
        } catch (final NoSuchElementException e) {

        }

        try {
            test.addEdgeValue(1, -4);
            fail("No such element exists");
        } catch (final NoSuchElementException e) {

        }
    }

    @Test
    public void testAddEdgeIndex() {
        test.addVertex(5);
        test.addEdgeIndex(0, 2);
        test.addEdgeIndex(2, 3);
        test.addEdgeIndex(1, 4);
        test.addEdgeIndex(0, 1);

        assertEquals("12534", test.dfSearch());
        assertEquals("12354", test.bfSearch());
    }

    @Test
    public void testAddEdgeIndexTwo() {
        try {
            test.addEdgeIndex(-1, 2);
            fail("Index is out of bounds");
        } catch (final IndexOutOfBoundsException e) {

        }

        try {
            test.addEdgeIndex(2, 100);
            fail("Index is out of bounds");
        } catch (final IndexOutOfBoundsException e) {

        }
    }

    @Test
    public void testDfSearch() {
        for (int i = 5; i <= 9; i++) {
            test.addVertex(i);
        }

        test.addEdgeIndex(0, 1);
        test.addEdgeIndex(1, 2);
        test.addEdgeIndex(0, 3);
        test.addEdgeIndex(3, 4);
        test.addEdgeIndex(0, 5);
        test.addEdgeIndex(5, 6);
        test.addEdgeIndex(0, 7);
        test.addEdgeIndex(7, 8);

        assertEquals("123456789", test.dfSearch());
    }

    @Test
    public void testBfSearch() {
        for (int i = 5; i <= 9; i++) {
            test.addVertex(i);
        }

        test.addEdgeIndex(0, 1);
        test.addEdgeIndex(1, 2);
        test.addEdgeIndex(0, 3);
        test.addEdgeIndex(3, 4);
        test.addEdgeIndex(0, 5);
        test.addEdgeIndex(5, 6);
        test.addEdgeIndex(0, 7);
        test.addEdgeIndex(7, 8);

        assertEquals("124683579", test.bfSearch());
    }

    @Test
    public void testDeleteIndex() {
        test.addVertex(5);
        test.addVertex(5);
        test.addEdgeIndex(0, 2);
        test.addEdgeIndex(2, 3);
        test.addEdgeIndex(1, 4);
        test.addEdgeIndex(0, 1);

        assertEquals("12534", test.dfSearch());
        assertEquals("12354", test.bfSearch());
        assertEquals(5, test.getAdj().length);

        test.deleteIndex(4);
        assertEquals("1234", test.dfSearch());
        assertEquals("1234", test.bfSearch());
        assertEquals(4, test.getAdj().length);
        assertEquals(4, test.getAdj()[0].length);

        test.deleteIndex(3);
        assertEquals("123", test.dfSearch());
        assertEquals("123", test.bfSearch());
        assertEquals(3, test.getAdj().length);

        test.deleteIndex(2);
        assertEquals("12", test.dfSearch());
        assertEquals("12", test.bfSearch());
        assertEquals(2, test.getAdj().length);

        test.deleteIndex(1);
        assertEquals("1", test.dfSearch());
        assertEquals("1", test.bfSearch());
        assertEquals(1, test.getAdj().length);

        test.deleteIndex(0);
        assertEquals("", test.dfSearch());
        assertEquals("", test.bfSearch());
        assertEquals(0, test.getAdj().length);
    }

    @Test
    public void testDeleteValue() {
        test.addVertex(5);
        test.addVertex(5);
        test.addEdgeIndex(0, 2);
        test.addEdgeIndex(2, 3);
        test.addEdgeIndex(1, 4);
        test.addEdgeIndex(0, 1);

        assertEquals("12534", test.dfSearch());
        assertEquals("12354", test.bfSearch());
        assertEquals(5, test.getAdj().length);

        test.deleteValue(5);
        assertEquals("1234", test.dfSearch());
        assertEquals("1234", test.bfSearch());
        assertEquals(4, test.getAdj().length);

        test.deleteValue(4);
        assertEquals("123", test.dfSearch());
        assertEquals("123", test.bfSearch());
        assertEquals(3, test.getAdj().length);

        test.deleteValue(3);
        assertEquals("12", test.dfSearch());
        assertEquals("12", test.bfSearch());
        assertEquals(2, test.getAdj().length);

        test.deleteValue(2);
        assertEquals("1", test.dfSearch());
        assertEquals("1", test.bfSearch());
        assertEquals(1, test.getAdj().length);

        test.deleteValue(1);
        assertEquals("", test.dfSearch());
        assertEquals("", test.bfSearch());
        assertEquals(0, test.getAdj().length);
    }

}
