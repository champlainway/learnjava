package net.smallbulletin.graph.util;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class DirectedGraphTest {

    private final DirectedGraph<Integer> test = new DirectedGraph<>();;

    @Before
    public void before() {
        test.addVertex(1);
        test.addVertex(2);
        test.addVertex(3);
        test.addVertex(4);
        test.addVertex(5);
    }

    @Test
    public void testMinimalTree() {
        test.addEdgeIndex(0, 1);
        test.addEdgeIndex(0, 2);
        test.addEdgeIndex(0, 3);
        test.addEdgeIndex(0, 4);
        test.addEdgeIndex(1, 2);
        test.addEdgeIndex(1, 3);
        test.addEdgeIndex(1, 4);
        test.addEdgeIndex(2, 3);
        test.addEdgeIndex(2, 4);
        test.addEdgeIndex(3, 4);

        assertEquals("12 23 34 45", test.minimalTree().trim());
    }

    @Test
    public void testTopographSort() {
        test.addVertex(6);
        test.addVertex(7);
        test.addVertex(8);

        test.addEdgeIndex(0, 3);
        test.addEdgeIndex(0, 4);
        test.addEdgeIndex(1, 4);
        test.addEdgeIndex(2, 5);
        test.addEdgeIndex(3, 6);
        test.addEdgeIndex(4, 6);
        test.addEdgeIndex(5, 7);
        test.addEdgeIndex(6, 7);

        assertEquals("86374512", test.topographSort());
    }

    @Test
    public void testValueIsReachable() {
        test.addEdgeIndex(0, 1);
        test.addEdgeIndex(1, 2);
        test.addEdgeIndex(2, 3);
        test.addEdgeIndex(3, 4);

        assertTrue(test.valueIsReachable(1, 5));
        assertTrue(test.valueIsReachable(2, 4));
        assertFalse(test.valueIsReachable(5, 1));
        assertFalse(test.valueIsReachable(4, 2));

    }

    @Test
    public void testIsReachableInt() {
        test.addEdgeIndex(0, 1);
        test.addEdgeIndex(1, 2);
        test.addEdgeIndex(2, 3);
        test.addEdgeIndex(3, 4);

        assertTrue(test.indexIsReachable(0, 4));
        assertTrue(test.indexIsReachable(1, 3));
        assertFalse(test.indexIsReachable(4, 0));
        assertFalse(test.indexIsReachable(3, 1));
    }

}
