package net.smallbulletin.bintree.redblack;

import static org.junit.Assert.*;

import org.junit.Test;

public class RBTreeTest {

    private final RBTree<Integer> tree = new RBTree<Integer>();

    @Test
    public void test() {
        tree.insert(1);
        assertEquals("(1, B)", tree.postOrder());

        tree.insert(2);
        assertEquals("(2, R)(1, B)", tree.postOrder());

        tree.insert(3);
        assertEquals("(1, R)(3, R)(2, B)", tree.postOrder());

        tree.insert(4);
        assertEquals("(1, B)(4, R)(3, B)(2, B)", tree.postOrder());

        tree.insert(5);
        assertEquals("(1, B)(3, R)(5, R)(4, B)(2, B)", tree.postOrder());

        tree.insert(6);
        assertEquals("(1, B)(3, B)(6, R)(5, B)(4, R)(2, B)", tree.postOrder());

        tree.insert(7);
        assertEquals("(1, B)(3, B)(5, R)(7, R)(6, B)(4, R)(2, B)", tree.postOrder());

    }

    @Test
    public void testTwo() {
        tree.insert(7);
        assertEquals("(7, B)", tree.postOrder());

        tree.insert(6);
        assertEquals("(6, R)(7, B)", tree.postOrder());

        tree.insert(5);
        assertEquals("(5, R)(7, R)(6, B)", tree.postOrder());

        tree.insert(4);
        assertEquals("(4, R)(5, B)(7, B)(6, B)", tree.postOrder());

        tree.insert(3);
        assertEquals("(3, R)(5, R)(4, B)(7, B)(6, B)", tree.postOrder());

        tree.insert(2);
        assertEquals("(2, R)(3, B)(5, B)(4, R)(7, B)(6, B)", tree.postOrder());

        tree.insert(1);
        assertEquals("(1, R)(3, R)(2, B)(5, B)(4, R)(7, B)(6, B)", tree.postOrder());
    }

    @Test
    public void testThree() {
        tree.insert(5);
        assertEquals("(5, B)", tree.postOrder());

        tree.insert(3);
        assertEquals("(3, R)(5, B)", tree.postOrder());

        tree.insert(1);
        assertEquals("(1, R)(5, R)(3, B)", tree.postOrder());

        tree.insert(2);
        assertEquals("(2, R)(1, B)(5, B)(3, B)", tree.postOrder());

        tree.insert(10);
        assertEquals("(2, R)(1, B)(10, R)(5, B)(3, B)", tree.postOrder());

        tree.insert(11);
        assertEquals("(2, R)(1, B)(5, R)(11, R)(10, B)(3, B)", tree.postOrder());

        tree.insert(9);
        assertEquals("(2, R)(1, B)(9, R)(5, B)(11, B)(10, R)(3, B)", tree.postOrder());

        tree.insert(8);
        assertEquals("(2, R)(1, B)(5, R)(9, R)(8, B)(11, B)(10, R)(3, B)", tree.postOrder());
    }

    @Test
    public void testDeleteOne() {
        tree.insert(3);
        tree.insert(5);
        tree.insert(1);
        tree.insert(6);
        tree.insert(8);
        tree.insert(9);
        tree.insert(7);
        tree.insert(10);

        assertEquals("(1, B)(5, B)(3, R)(7, B)(10, R)(9, B)(8, R)(6, B)", tree.postOrder());

        tree.delete(8);

        assertEquals("(1, B)(5, B)(3, R)(7, B)(10, R)(9, B)(6, B)", tree.postOrder());
    }

    @Test
    public void testDeleteTwo() {
        tree.insert(1);
        tree.insert(2);
        tree.insert(3);

        tree.delete(3);

        assertEquals("(1, R)(2, B)", tree.postOrder());
    }

    @Test
    public void testDeleteThree() {
        tree.insert(1);
        tree.insert(2);
        tree.insert(3);
        tree.insert(4);

        tree.delete(1);
        assertEquals("(2, B)(4, B)(3, B)", tree.postOrder());
    }

    @Test
    public void testDeleteFour() {
        tree.insert(1);
        tree.insert(3);
        tree.insert(5);
        tree.insert(4);
        assertEquals("(1, B)(4, R)(5, B)(3, B)", tree.postOrder());

        tree.delete(5);
        assertEquals("(1, B)(4, B)(3, B)", tree.postOrder());

    }

    @Test
    public void testDeleteFive() {
        tree.insert(1);
        tree.insert(3);
        tree.insert(5);
        tree.insert(2);
        assertEquals("(2, R)(1, B)(5, B)(3, B)", tree.postOrder());
        tree.delete(1);
        assertEquals("(2, B)(5, B)(3, B)", tree.postOrder());

    }

    @Test
    public void testDeleteSix() {
        tree.insert(1);
        tree.insert(2);
        tree.insert(3);
        tree.insert(4);
        tree.insert(6);

        assertEquals("(1, B)(3, R)(6, R)(4, B)(2, B)", tree.postOrder());

        tree.delete(4);
        assertEquals("(1, B)(3, R)(6, B)(2, B)", tree.postOrder());
    }

    @Test
    public void testDeleteSeven() {
        tree.insert(1);
        tree.insert(2);
        tree.insert(3);
        tree.insert(4);
        tree.insert(6);
        tree.insert(5);
        tree.insert(7);

        assertEquals("(1, B)(3, B)(5, R)(7, R)(6, B)(4, R)(2, B)", tree.postOrder());

        tree.delete(4);
        assertEquals("(1, B)(3, B)(7, R)(6, B)(5, R)(2, B)", tree.postOrder());
    }

    @Test
    public void testDeleteEight() {
        tree.insert(1);
        tree.insert(2);
        tree.insert(3);
        assertEquals("(1, R)(3, R)(2, B)", tree.postOrder());

        tree.delete(2);

        assertEquals("(1, R)(3, B)", tree.postOrder());

    }

    @Test
    public void testDeleteNine() {
        tree.insert(1);
        tree.insert(2);
        tree.insert(3);
        tree.insert(4);
        tree.insert(6);
        tree.insert(5);
        tree.insert(7);

        assertEquals("(1, B)(3, B)(5, R)(7, R)(6, B)(4, R)(2, B)", tree.postOrder());
        tree.delete(2);

        assertEquals("(1, B)(5, R)(7, R)(6, B)(4, R)(3, B)", tree.postOrder());

    }

    @Test
    public void testDeleteTen() {
        tree.insert(1);
        tree.insert(2);
        tree.insert(3);
        tree.insert(4);
        tree.insert(6);
        tree.insert(5);
        tree.insert(7);

        assertEquals("(1, B)(3, B)(5, R)(7, R)(6, B)(4, R)(2, B)", tree.postOrder());

        tree.delete(1);
        assertEquals("(2, B)(3, R)(5, R)(7, R)(6, B)(4, B)", tree.postOrder());
        tree.delete(2);
        assertEquals("(3, R)(5, R)(7, R)(6, B)(4, B)", tree.postOrder());
        tree.delete(3);
        assertEquals("(5, R)(7, R)(6, B)(4, B)", tree.postOrder());
        tree.delete(4);
        assertEquals("(5, R)(7, R)(6, B)", tree.postOrder());
        tree.delete(5);
        assertEquals("(7, R)(6, B)", tree.postOrder());
        tree.delete(6);
        assertEquals("(7, B)", tree.postOrder());
        tree.delete(7);
        assertEquals("", tree.postOrder());
    }

    @Test
    public void testDeleteEleven() {
        tree.insert(1);
        tree.insert(2);
        tree.insert(3);
        tree.insert(4);
        tree.insert(6);
        tree.insert(5);
        tree.insert(7);

        assertEquals("(1, B)(3, B)(5, R)(7, R)(6, B)(4, R)(2, B)", tree.postOrder());

        tree.delete(7);
        assertEquals("(2, B)(1, B)(4, R)(3, B)(6, B)(5, R)(null, B)", tree.level());

        tree.delete(6);
        assertEquals("(2, B)(1, B)(4, R)(3, B)(5, B)", tree.level());
        tree.delete(5);
        assertEquals("(2, B)(1, B)(3, R)(null, B)(4, B)", tree.level());
        tree.delete(4);
        assertEquals("(2, B)(1, B)(3, R)", tree.level());
        tree.delete(3);
        assertEquals("(2, B)(1, B)(null, B)", tree.level());
        tree.delete(2);
        assertEquals("(1, B)", tree.level());
        tree.delete(1);
        assertEquals("", tree.level());
    }

}
