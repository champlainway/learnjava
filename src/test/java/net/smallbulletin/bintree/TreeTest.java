package net.smallbulletin.bintree;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import net.smallbulletin.bintree.Tree;
import net.smallbulletin.bintree.TreeNode;

public class TreeTest {
    Tree<String> tree = new Tree<String>();
    TreeNode<String> root;

    @Before
    public void before() {
        tree.insert("M");
        tree.insert("B");
        tree.insert("X");
        tree.insert("Z");
        tree.insert("G");
        tree.insert("A");

        root = tree.getRoot();
    }

    @Test
    public void testGetRoot() {

        assertEquals("M", root.getValue());
    }

    @Test
    public void testInsert() {

        tree.insert("D");
        assertTrue(tree.contains("D"));
    }

    @Test
    public void testFind() {
        tree.insert("D");
        final TreeNode<String> find = tree.find("D");

        assertEquals("D", find.getValue());
    }

    @Test
    public void testContains() {
        tree.insert("D");
        assertTrue(tree.contains("D"));
        assertFalse(tree.contains("N"));
    }

    @Test
    public void testDeleteOne() {
        tree.delete("M");
        assertEquals("X", tree.getRoot().getValue());
        assertFalse(tree.contains("M"));
        assertEquals("ABGXZ", tree.inOrder());
    }

    @Test
    public void testDeleteTwo() {
        tree.delete("X");

        System.out.println(tree.inOrder());

        final TreeNode<String> node = root.getRight();
        assertEquals("Z", node.getValue());
        assertFalse(tree.contains("X"));
        assertEquals("ABGMZ", tree.inOrder());
    }

    @Test
    public void testDeleteThree() {
        tree.delete("A");
        assertNull(root.getLeft().getLeft());
        assertFalse(tree.contains("A"));
        assertEquals("BGMXZ", tree.inOrder());
    }

    @Test
    public void testDeleteFour() {
        final Tree<String> newTree = new Tree<>();
        newTree.insert("M");
        newTree.insert("A");
        newTree.insert("X");
        newTree.insert("Z");

        newTree.delete("X");
        final TreeNode<String> newRoot = newTree.getRoot();
        assertNull(newRoot.getRight().getRight());
        assertFalse(newTree.contains("X"));
        assertEquals("AMZ", newTree.inOrder());
    }

    public void testDeleteFive() {
        tree.delete("B");

        assertEquals("AGZXM", tree.postOrder());
    }

    @Test
    public void testInOrder() {
        final String inOrder = tree.inOrder();
        assertEquals("ABGMXZ", inOrder);
    }

    @Test
    public void testPreOrder() {
        final String preOrder = tree.preOrder();
        assertEquals("MBAGXZ", preOrder);
    }

    @Test
    public void testPostOrder() {
        final String postOrder = tree.postOrder();
        assertEquals("AGBZXM", postOrder);

    }

    @Test
    public void testReverseOrder() {
        final String reverseOrder = tree.reverseOrder();
        assertEquals("ZXMAGB", reverseOrder);
    }

    @Test
    public void testGetHeight() {
        final int height = tree.getHeight();

        assertEquals(3, height);
    }

    @Test
    public void testLevel() {
        final String str = tree.level();

        assertEquals("MBXAG Z", str);
    }

}
