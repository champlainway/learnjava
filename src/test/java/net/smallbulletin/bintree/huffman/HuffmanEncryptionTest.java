package net.smallbulletin.bintree.huffman;

import static org.junit.Assert.*;
import org.junit.Test;

public class HuffmanEncryptionTest {

    @Test
    public void testEncrypt() {
        verifyEncryption("");
        verifyEncryption("A");
        verifyEncryption("AAAAAAAAAAA");
        verifyEncryption("SUSIE SAYS IT IS EASY");
        verifyEncryption("Susie says it is easy!");
    }

    private void verifyEncryption(final String str) {
        final HuffmanEncryption encryption = new HuffmanEncryption(str);
        final String encrypted = encryption.encrypt();
        assertEquals(str, encryption.decrypt(encrypted));
    }

    @Test
    public void testDecrypt() {
        final HuffmanEncryption encryption = new HuffmanEncryption("HELLO");
        final String encrypted = encryption.encrypt();
        assertEquals("HELLOHELLO", encryption.decrypt(encrypted + encrypted));
    }

    @Test
    public void testBitSize() {
        final String str = "HELLLLO";
        final HuffmanEncryption encryption = new HuffmanEncryption(str);
        assertEquals("L", encryption.decrypt("1"));
        System.out.println( 8.0 * str.length() / encryption.encrypt().length());
    }

}
