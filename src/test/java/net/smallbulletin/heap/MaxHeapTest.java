package net.smallbulletin.heap;

import static org.junit.Assert.*;

import org.junit.Test;

public class MaxHeapTest {

    MaxHeap<Integer> heap = new MaxHeap<Integer>();

    @Test
    public void testRemove() {
        heap.insert(5);
        heap.insert(4);
        heap.insert(10);
        heap.insert(3);
        heap.insert(2);
        heap.insert(6);
        heap.insert(8);

        assertEquals(Integer.valueOf(10), heap.remove());
        assertEquals(Integer.valueOf(8), heap.remove());
        assertEquals(Integer.valueOf(6), heap.remove());
        assertEquals(Integer.valueOf(5), heap.remove());
        assertEquals(Integer.valueOf(4), heap.remove());
        assertEquals(Integer.valueOf(3), heap.remove());
        assertEquals(Integer.valueOf(2), heap.remove());
    }

    @Test
    public void testRemoveTwo() {
        for (int i = 1; i <= 10; i++) {
            heap.insert(i);
        }

        for (int i = 10; i >= 1; i--) {
            assertEquals(Integer.valueOf(i), heap.remove());
        }
    }

}
