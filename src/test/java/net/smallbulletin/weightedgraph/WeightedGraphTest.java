package net.smallbulletin.weightedgraph;

import static org.junit.Assert.*;

import org.junit.Test;

public class WeightedGraphTest {
    WeightedGraph<Integer> graph = new WeightedGraph<>();

    @Test
    public void testMinimalTree() {
        graph.addVertex(1);
        graph.addVertex(2);
        graph.addVertex(3);
        graph.addVertex(4);

        graph.addEdgeIndex(0, 1, 20);
        graph.addEdgeIndex(0, 2, 10);
        graph.addEdgeIndex(0, 3, 15);
        graph.addEdgeIndex(1, 2, 20);
        graph.addEdgeIndex(1, 3, 18);
        graph.addEdgeIndex(2, 3, 14);

        assertEquals("1-3 3-4 4-2", graph.getMST());
    }

    @Test
    public void testMinTreeTwo() {
        assertEquals("", graph.getMST());
    }

    @Test
    public void testMinTreeThree() {
        graph.addVertex(1);
        assertEquals("", graph.getMST());
    }

    @Test
    public void testMinTreeFour() {
        graph.addVertex(1);
        graph.addVertex(2);
        graph.addVertex(3);
        graph.addVertex(4);

        graph.addEdgeIndex(0, 1, 5);
        graph.addEdgeIndex(2, 3, 5);

        assertEquals("", graph.getMST());
    }

    @Test
    public void testMinTreeFive() {
        graph.addVertex(1);
        graph.addVertex(2);
        graph.addEdgeIndex(0, 1, 10);

        assertEquals("1-2", graph.getMST());
    }

}
