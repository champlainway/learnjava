package net.smallbulletin.weightedgraph;

import static org.junit.Assert.*;

import org.junit.Test;

public class WDGraphTest {
    WDGraph<Integer> graph = new WDGraph<>();

    @Test
    public void testDijkstra() {
        graph.addVertex(0);
        graph.addVertex(1);
        graph.addVertex(2);
        graph.addVertex(3);
        graph.addVertex(4);

        graph.addEdgeIndex(0, 1, 50);
        graph.addEdgeIndex(0, 3, 80);
        graph.addEdgeIndex(1, 2, 60);
        graph.addEdgeIndex(1, 3, 90);
        graph.addEdgeIndex(2, 4, 40);
        graph.addEdgeIndex(3, 2, 20);
        graph.addEdgeIndex(3, 4, 70);
        graph.addEdgeIndex(4, 1, 50);

        assertEquals("0=INF(from 0) 1=50(from 0) 2=100(from 3) 3=80(from 0) 4=140(from 2)", graph.dijkstra());
    }

}
