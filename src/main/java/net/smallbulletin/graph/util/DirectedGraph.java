package net.smallbulletin.graph.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Queue;
import java.util.Stack;

public class DirectedGraph<E extends Comparable<E>> {
    private List<Vertex<E>> graph;
    private int[][] adj;

    public DirectedGraph() {
        graph = new ArrayList<Vertex<E>>();

        adj = new int[0][0];
    }

    public void addVertex(final E newVal) {
        if (!containsValue(newVal)) {
            final Vertex<E> newVertex = new Vertex<>(newVal);

            graph.add(newVertex);
            adj = expandMatrix();
        }
    }

    public void addEdgeIndex(final int a, final int b) {
        if (a < 0 || a >= graph.size()) {
            throw new IndexOutOfBoundsException(a + " is out of bounds");
        }

        if (b < 0 || b >= graph.size()) {
            throw new IndexOutOfBoundsException(b + " is out of bounds");
        }

        adj[a][b] = 1;
    }

    public void addEdgeValue(final E a, final E b) {

        if (!containsValue(a)) {
            throw new NoSuchElementException("No element: " + a);
        }

        if (!containsValue(b)) {
            throw new NoSuchElementException("No element: " + b);
        }

        final int indexA = vertexWithValue(a);
        final int indexB = vertexWithValue(b);

        addEdgeIndex(indexA, indexB);
    }

    public String dfSearch() {
        String result = "";
        final Stack<Vertex<E>> stack = new Stack<>();
        stack.push(graph.get(0));

        while (!stack.isEmpty()) {
            final Vertex<E> curr = stack.peek();
            final int possAdj = getAdjUnvisited(curr.getValue());

            if (possAdj == -1) {
                stack.pop();
            } else {
                curr.setVisited(true);
                result += curr.getValue();
                stack.push(graph.get(possAdj));
            }
        }
        resetVisit();
        return result;
    }

    public String bfSearch() {
        String result = "";
        final Queue<Vertex<E>> queue = new LinkedList<>();
        result += graph.get(0).getValue();
        graph.get(0).setVisited(true);
        queue.add(graph.get(0));

        while (!queue.isEmpty()) {
            final Vertex<E> curr = queue.remove();
            final int possAdj = getAdjUnvisited(curr.getValue());

            while (possAdj != -1) {
                final Vertex<E> tmp = graph.get(possAdj);
                tmp.setVisited(true);
                result += tmp.getValue();
                queue.add(tmp);
            }
        }

        resetVisit();
        return result;
    }

    public String minimalTree() {
        String result = "";
        final Stack<Vertex<E>> stack = new Stack<>();
        final Vertex<E> root = graph.get(0);
        root.setVisited(true);
        stack.push(root);

        while (!stack.isEmpty()) {
            final Vertex<E> curr = stack.peek();
            final int possAdj = getAdjUnvisited(curr.getValue());

            if (possAdj == -1) {
                stack.pop();
            } else {
                final Vertex<E> tmp = graph.get(possAdj);
                tmp.setVisited(true);
                stack.push(tmp);
                result += curr.getValue();
                result += graph.get(possAdj).getValue();
                result += " ";
            }
        }

        resetVisit();
        return result;
    }

    public String topographSort() {
        String result = "";
        final List<Vertex<E>> saved = saveGraph();
        final int[][] adjSaved = saveAdj();

        while (graph.size() > 0) {
            final int curr = noSuccessor();

            if (curr == -1) {
                return null;
            }

            final Vertex<E> tmp = graph.get(curr);

            result = result + tmp.getValue();
            deleteIndex(curr);
        }

        graph = saved;
        adj = adjSaved;
        return result;
    }

    public void deleteValue(final E e) {
        final int index = vertexWithValue(e);
        deleteIndex(index);
    }

    public void deleteIndex(final int index) {
        final int length = adj.length;
        final int[][] newAdj = new int[length - 1][length - 1];

        for (int i = 0; i < length - 1; i++) {
            for (int j = 0; j < length - 1; j++) {
                if (i >= index && j >= index) {
                    newAdj[i][j] = adj[i + 1][j + 1];
                } else if (i >= index) {
                    newAdj[i][j] = adj[i + 1][j];
                } else if (j >= index) {
                    newAdj[i][j] = adj[i][j + 1];
                } else {
                    newAdj[i][j] = adj[i][j];
                }
            }
        }

        adj = newAdj;
        graph.remove(index);
    }

    public boolean valueIsReachable(final E start, final E end) {
        final int a = vertexWithValue(start);
        final int b = vertexWithValue(end);

        return indexIsReachable(a, b);
    }

    public boolean indexIsReachable(final int start, final int end) {
        final int[][] tmpAdj = warshall();

        return tmpAdj[start][end] == 1;
    }

    private int vertexWithValue(final E e) {

        for (int i = 0; i < graph.size(); i++) {
            final Vertex<E> tmp = graph.get(i);

            if (tmp.getValue().equals(e)) {
                return i;
            }
        }

        return -1;
    }

    private int[][] expandMatrix() {
        final int newSize = adj.length + 1;
        final int[][] toReturn = new int[newSize][newSize];

        for (int i = 0; i < adj.length; i++) {
            final int[] tmp = Arrays.copyOf(adj[i], newSize);
            toReturn[i] = tmp;
        }

        return toReturn;
    }

    private int getAdjUnvisited(final E val) {
        final int index = vertexWithValue(val);

        final int[] adjVerts = adj[index];

        for (int i = 0; i < adjVerts.length; i++) {
            if (adjVerts[i] == 1 && !graph.get(i).isVisited()) {
                return i;
            }
        }

        return -1;
    }

    private boolean containsValue(final E val) {
        return vertexWithValue(val) != -1;
    }

    private void resetVisit() {
        for (final Vertex<E> curr : graph) {
            curr.setVisited(false);
        }
    }

    private int noSuccessor() {
        for (int i = 0; i < adj.length; i++) {
            if (hasNoSuccessors(adj[i])) {
                return i;
            }
        }

        return -1;
    }

    private boolean hasNoSuccessors(final int[] curr) {
        for (int i = 0; i < curr.length; i++) {
            if (curr[i] != 0) {
                return false;
            }
        }

        return true;
    }

    private List<Vertex<E>> saveGraph() {
        final List<Vertex<E>> tmp = new ArrayList<>();

        for (int i = 0; i < graph.size(); i++) {
            tmp.add(graph.get(i));
        }

        return tmp;
    }

    private int[][] saveAdj() {
        final int[][] tmp = new int[adj.length][adj.length];

        for (int i = 0; i < adj.length; i++) {
            tmp[i] = adj[i];
        }

        return tmp;
    }

    private int[][] warshall() {
        final int[][] toReturn = Arrays.copyOf(adj, adj.length);

        for (int i = 0; i < toReturn.length; i++) {
            for (int j = 0; j < toReturn.length; j++) {
                final int curr = toReturn[i][j];

                if (curr == 1) {
                    for (int k = 0; k < toReturn.length; k++) {
                        if (toReturn[k][i] == 1) {
                            toReturn[k][j] = 1;
                        }
                    }
                }
            }
        }

        return toReturn;
    }

    Vertex<E> get(final int index) {
        return graph.get(index);
    }

    List<Vertex<E>> getGraph() {
        return graph;
    }

    int[][] getAdj() {
        return adj;
    }

}
