package net.smallbulletin.graph.util;

import java.util.ArrayList;
import java.util.List;

import jersey.repackaged.com.google.common.collect.Lists;

public class KnightTour {
    private Graph<Integer> board;
    int size;

    public KnightTour(final int size) {
        this.size = size;
        createBoard();
    }

    public void generatePath(final int index) {
        final List<Integer> path = new ArrayList<>();
        path.add(index);
        final List<Integer> completed = generatePath(path);
        for (int i = 0; i < completed.size(); i++) {
            System.out.print(completed.get(i) + " ");

        }
    }

    private List<Integer> generatePath(final List<Integer> partial) {

        System.out.println("Trying: " + partial);

        if (partial.isEmpty()) {
            return partial;
        }

        final int lastVal = partial.get(partial.size() - 1);
        final List<Integer> adj = board.getAdjacent(lastVal);

        for (final int potential : adj) {
            if (partial.contains(potential)) {
                continue;
            }
            final List<Integer> tmp = Lists.newArrayList(partial);
            tmp.add(potential);

            final List<Integer> path = generatePath(tmp);

            if (path.size() == size * size) {
                return path;
            }
        }

        return partial;
    }

    private void createBoard() {
        board = new Graph<Integer>();

        for (int i = 0; i < size * size; i++) {
            board.addVertex(i);
        }

        for (int i = 0; i < size * size; i++) {
            final int r = i / size;
            final int c = i % size;

            final int tl = rcToIndex(r - 2, c - 1);
            final int tr = rcToIndex(r - 2, c + 1);
            final int tmr = rcToIndex(r - 1, c + 2);
            final int bmr = rcToIndex(r + 1, c + 2);
            final int br = rcToIndex(r + 2, c + 1);
            final int bl = rcToIndex(r + 2, c - 1);
            final int bml = rcToIndex(r + 1, c - 2);
            final int tml = rcToIndex(r - 1, c - 2);

            if (tl >= 0 && tl < size * size) {
                board.addEdgeIndex(i, tl);
            }

            if (tr >= 0 && tr < size * size) {
                board.addEdgeIndex(i, tr);
            }

            if (tmr >= 0 && tmr < size * size) {
                board.addEdgeIndex(i, tmr);
            }

            if (bmr >= 0 && bmr < size * size) {
                board.addEdgeIndex(i, bmr);
            }

            if (br >= 0 && br < size * size) {
                board.addEdgeIndex(i, br);
            }

            if (bl >= 0 && bl < size * size) {
                board.addEdgeIndex(i, bl);
            }

            if (bml >= 0 && bml < size * size) {
                board.addEdgeIndex(i, bml);
            }

            if (tml >= 0 && tml < size * size) {
                board.addEdgeIndex(i, tml);
            }
        }
    }

    private int rcToIndex(final int r, final int c) {
        if (inBounds(r) && inBounds(c)) {
            return size * r + c;
        } else {
            return -1;
        }
    }

    private boolean inBounds(final int i) {
        return i >= 0 && i < size;
    }

}
