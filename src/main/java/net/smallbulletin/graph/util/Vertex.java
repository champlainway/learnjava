package net.smallbulletin.graph.util;

public class Vertex<E extends Comparable<E>> {
    private E value;
    private boolean visited;

    public Vertex() {

    }

    public Vertex(final E value) {
        this.value = value;
    }

    public E getValue() {
        return value;
    }

    public boolean isVisited() {
        return visited;
    }

    public void setVisited(final boolean visited) {
        this.visited = visited;
    }

    @Override
    public String toString() {
        return value + "";
    }
}
