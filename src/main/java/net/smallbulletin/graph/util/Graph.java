package net.smallbulletin.graph.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Queue;
import java.util.Stack;

public class Graph<E extends Comparable<E>> {
    private final List<Vertex<E>> graph;
    private int[][] adj;

    public Graph() {
        graph = new ArrayList<Vertex<E>>();

        adj = new int[0][0];
    }

    public void addVertex(final E newVal) {
        if (!containsValue(newVal)) {
            final Vertex<E> newVertex = new Vertex<>(newVal);

            graph.add(newVertex);
            adj = expandMatrix();
        }
    }

    public void addEdgeIndex(final int a, final int b) {
        if (a < 0 || a >= graph.size()) {
            throw new IndexOutOfBoundsException(a + " is out of bounds");
        }

        if (b < 0 || b >= graph.size()) {
            throw new IndexOutOfBoundsException(b + " is out of bounds");
        }

        adj[a][b] = 1;
        adj[b][a] = 1;
    }

    public void addEdgeValue(final E a, final E b) {

        if (!containsValue(a)) {
            throw new NoSuchElementException("No element: " + a);
        }

        if (!containsValue(b)) {
            throw new NoSuchElementException("No element: " + b);
        }

        final int indexA = vertexWithValue(a);
        final int indexB = vertexWithValue(b);

        addEdgeIndex(indexA, indexB);
    }

    public String dfSearch() {
        String result = "";

        if (graph.size() == 0) {
            return result;
        }

        final Stack<Vertex<E>> stack = new Stack<>();
        final Vertex<E> root = graph.get(0);
        root.setVisited(true);
        result += root.getValue();
        stack.push(root);

        while (!stack.isEmpty()) {
            final Vertex<E> curr = stack.peek();
            final int possAdj = getAdjUnvisited(curr.getValue());

            if (possAdj == -1) {
                stack.pop();
            } else {
                final Vertex<E> tmp = graph.get(possAdj);

                tmp.setVisited(true);
                result += tmp.getValue();
                stack.push(tmp);
            }
        }
        resetVisit();
        return result;
    }

    public String bfSearch() {
        String result = "";

        if (graph.size() == 0) {
            return result;
        }

        final Queue<Vertex<E>> queue = new LinkedList<>();
        result += graph.get(0).getValue();
        graph.get(0).setVisited(true);
        queue.add(graph.get(0));

        while (!queue.isEmpty()) {
            final Vertex<E> curr = queue.remove();
            int possAdj = getAdjUnvisited(curr.getValue());

            while (possAdj != -1) {
                final Vertex<E> tmp = graph.get(possAdj);
                tmp.setVisited(true);
                result += tmp.getValue();
                queue.add(tmp);
                possAdj = getAdjUnvisited(curr.getValue());
            }
        }

        resetVisit();
        return result;
    }

    public String minimalTree() {
        String result = "";
        final Stack<Vertex<E>> stack = new Stack<>();
        stack.push(graph.get(0));

        while (!stack.isEmpty()) {
            final Vertex<E> curr = stack.peek();
            final int possAdj = getAdjUnvisited(curr.getValue());

            if (possAdj == -1) {
                stack.pop();
            } else {
                curr.setVisited(true);
                stack.push(graph.get(possAdj));
                result += curr.getValue();
                result += graph.get(possAdj).getValue();
            }
        }
        resetVisit();
        return result;
    }

    public void deleteValue(final E e) {
        final int index = vertexWithValue(e);
        deleteIndex(index);
    }

    public void deleteIndex(final int index) {
        final int length = adj.length;
        final int[][] newAdj = new int[length - 1][length - 1];

        for (int i = 0; i < length - 1; i++) {
            for (int j = 0; j < length - 1; j++) {
                if (i >= index && j >= index) {
                    newAdj[i][j] = adj[i + 1][j + 1];
                } else if (i >= index) {
                    newAdj[i][j] = adj[i + 1][j];
                } else if (j >= index) {
                    newAdj[i][j] = adj[i][j + 1];
                } else {
                    newAdj[i][j] = adj[i][j];
                }
            }
        }

        adj = newAdj;
        graph.remove(index);
    }

    public List<E> getAdjacent(final E val) {
        final int index = vertexWithValue(val);
        final List<E> list = new ArrayList<>();

        for (int i = 0; i < adj[index].length; i++) {
            if (adj[index][i] == 1) {
                list.add(get(i).getValue());
            }
        }

        return list;
    }

    private int getAdjUnvisited(final E val) {
        final int index = vertexWithValue(val);

        final int[] adjVerts = adj[index];

        for (int i = 0; i < adjVerts.length; i++) {
            if (adjVerts[i] == 1 && !graph.get(i).isVisited()) {
                return i;
            }
        }

        return -1;
    }

    private int vertexWithValue(final E e) {

        for (int i = 0; i < graph.size(); i++) {
            final Vertex<E> tmp = graph.get(i);

            if (tmp.getValue().equals(e)) {
                return i;
            }
        }

        return -1;
    }

    private int[][] expandMatrix() {
        final int newSize = adj.length + 1;
        final int[][] toReturn = new int[newSize][newSize];

        for (int i = 0; i < adj.length; i++) {
            final int[] tmp = Arrays.copyOf(adj[i], newSize);
            toReturn[i] = tmp;
        }

        return toReturn;
    }

    private boolean containsValue(final E val) {
        return vertexWithValue(val) != -1;
    }

    private void resetVisit() {
        for (final Vertex<E> curr : graph) {
            curr.setVisited(false);
        }
    }

    Vertex<E> get(final int index) {
        return graph.get(index);
    }

    List<Vertex<E>> getGraph() {
        return graph;
    }

    int[][] getAdj() {
        return adj;
    }

}
