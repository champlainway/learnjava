package net.smallbulletin.recursionapp;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class HanoiOutput {
    private List<String> pics;

    @JsonCreator
    public HanoiOutput(@JsonProperty("pics") List<String> pics) {
        this.pics = pics;
    }

    public List<String> getPics() {
        return pics;
    }

    public void setPics(List<String> pics) {
        this.pics = pics;
    }

}
