package net.smallbulletin.recursionapp;

public class SearchOutput {
    private int index;
    private String[] array;

    public SearchOutput(final int index, final String[] array) {
        this.index = index;
        this.array = array;
    }

    public int getIndex() {
        return this.index;
    }

    public void setIndex(final int index) {
        this.index = index;
    }

    public String[] getArray() {
        return this.array;
    }

    public void setArray(final String[] array) {
        this.array = array;
    }

}
