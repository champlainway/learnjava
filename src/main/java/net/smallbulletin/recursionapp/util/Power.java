package net.smallbulletin.recursionapp.util;

public class Power {

    public static double power(final long base, final long power) {

        if (power < 0) {
            return 1 / power(base, -1 * power);
        }

        if (power == 0) {
            return 1;
        }

        if (power == 1) {
            return base;
        }

        if (power % 2 == 1) {
            return base * power(base * base, power / 2);
        }

        return power(base * base, power / 2);
    }

}
