package net.smallbulletin.recursionapp.util;

import java.util.Stack;

public class TriangleStack {
    private final Stack<Long> nums = new Stack<>();

    public TriangleStack(final long n) {
        for (long i = n; i >= 0; i--) {
            nums.push(i);
        }
    }

    public long getNums() {

        long sum = 0;

        while (!nums.isEmpty()) {
            sum += nums.pop();
        }

        return sum;
    }
}
