package net.smallbulletin.recursionapp.util;

import java.util.Set;

import com.google.common.base.Strings;

import jersey.repackaged.com.google.common.collect.Sets;

public class Anagram {

    public static Set<String> anagram(final String str) {
        final Set<String> anagrammed = Sets.newHashSet();
        anagram(anagrammed, "", str);
        return anagrammed;
    }

    private static void anagram(Set<String> anagrammed, final String building, final String remaining) {
        if (Strings.isNullOrEmpty(remaining)) {
            anagrammed.add(building);
            return;
        }

        for (int i = 0; i < remaining.length(); i++) {
            final String newBuilding = building + remaining.charAt(i);
            final String newRemaining = remaining.substring(0, i) + remaining.substring(i + 1);

            anagram(anagrammed, newBuilding, newRemaining);
        }
    }

}
