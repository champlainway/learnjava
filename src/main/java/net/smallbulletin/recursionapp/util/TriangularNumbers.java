package net.smallbulletin.recursionapp.util;

public class TriangularNumbers {
	public static long getTriangular(final long n) {
		if (n == 1) {
			return 1;
		}

		return n + getTriangular(n - 1);
	}
}
