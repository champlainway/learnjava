package net.smallbulletin.recursionapp.util;

public class Factorial {

	public static long factorial(final long n) {
		if (n == 0) {
			return 1;
		}

		return n * factorial(n - 1);
	}
}
