package net.smallbulletin.recursionapp.util;

import java.util.List;

import jersey.repackaged.com.google.common.collect.Lists;

public class SelectTeam {

    public static List<String> run(final List<String> members, final int numOfPeople) {
        final List<String> answers = Lists.newArrayList();
        chooseTeam(members, 0, numOfPeople, "", answers);
        return answers;
    }

    private static boolean chooseTeam(final List<String> members, final int index, final int numOfPeople, final String curr,
            final List<String> answers) {
        if (numOfPeople < 1 || index >= members.size()) {
            return false;
        }

        if (numOfPeople == 1) {
            for (int i = index; i < members.size(); i++) {
                answers.add(curr + members.get(i));
            }

            return true;
        }

        for (int i = index; i < members.size(); i++) {
            final String tmp = members.get(i);
            if (numOfPeople == 1) {
                answers.add(curr + members.get(i));
            }
            chooseTeam(members, i + 1, numOfPeople - 1, curr + tmp, answers);

        }

        return false;
    }
}
