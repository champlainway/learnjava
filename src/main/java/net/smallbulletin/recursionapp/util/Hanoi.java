package net.smallbulletin.recursionapp.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class Hanoi {

    private Stack<Integer> left = new Stack<>();
    private Stack<Integer> middle = new Stack<>();
    private Stack<Integer> right = new Stack<>();
    private List<String> pics = new ArrayList<>();
    private int size = 0;

    public Hanoi() {

    }

    public Hanoi(int n) {
        for (int i = n; i > 0; i--) {
            left.push(i);
        }
    }

    public void run() {
        size = left.size();
        hanoi(left, middle, right, left.size());
    }

    private void hanoi(Stack<Integer> a, Stack<Integer> b, Stack<Integer> c, int n) {
        if (n == 1) {
            int curr = a.pop();
            c.push(curr);
            getList(left, middle, right, size, a, c, curr);
        } else {
            hanoi(a, c, b, n - 1);
            hanoi(a, b, c, 1);
            hanoi(b, a, c, n - 1);

        }

    }

    // gets line descriptions
    private String getDescrip(String diskOne, String diskTwo, int disk) {

        String descrip = "Disk " + disk + " from Tower " + diskOne + " to Tower " + diskTwo;

        return descrip;
    }

    private void getList(Stack<Integer> a, Stack<Integer> b, Stack<Integer> c, int n, Stack<Integer> towOne,
            Stack<Integer> towTwo, int disk) {

        StringBuilder str = new StringBuilder();
        String line = "";
        String from = "";
        String to = "";

        for (int i = n - 1; i >= 0; i--) {
            String currA = valueAtHeight(a, i);
            String currB = valueAtHeight(b, i);
            String currC = valueAtHeight(c, i);

            // System.out.println(printDisk(currA, n) + " " + printDisk(currB,
            // n) + " " + printDisk(currC, n));
            str.append(printDisk(currA, n) + " " + printDisk(currB, n) + " " + printDisk(currC, n) + "\n");
        }
        // System.out.println(printBase(n));

        str.append(printBase(n) + "\n");

        // System.out.println();

        // try {
        // Thread.sleep(200);
        // } catch (InterruptedException e) {
        // // TODO Auto-generated catch block
        // e.printStackTrace();
        // }

        if (towOne == left) {
            from = "A";
        } else if (towOne == middle) {
            from = "B";
        } else {
            from = "C";
        }

        if (towTwo == left) {
            to = "A";
        } else if (towTwo == middle) {
            to = "B";
        } else {
            to = "C";
        }

        line = getDescrip(from, to, disk);

        str.append(line);
        str.append("\n");
        str.append("\n");

        pics.add(str.toString());
    }

    private String valueAtHeight(Stack<Integer> stack, int h) {
        if (stack.size() - 1 < h) {
            return "";
        }

        return stack.get(h).toString();
    }

    private String printBase(int n) {
        String toPrint = "";
        String spaces = "";
        for (int i = 1; i <= 2 * n; i++) {
            spaces += "x";
        }

        toPrint = spaces + "A" + spaces + " " + spaces + "B" + spaces + " " + spaces + "C" + spaces;

        return toPrint;
    }

    private String printDisk(String val, int n) {
        String spaces = "";
        String toPrint = "";
        if (val.length() == 0) {

            for (int i = 1; i <= 2 * n; i++) {
                spaces += " ";
            }

            toPrint = spaces + "|" + spaces;
            return toPrint;
        }

        int value = Integer.parseInt(val);
        String disk = "";

        for (int i = 1; i <= 2 * value - 1; i++) {
            disk += "=";
        }

        toPrint = "(" + disk + "|" + disk + ")";

        for (int i = 1; i <= 2 * n - 2 * value; i++) {
            spaces += " ";
        }

        toPrint = spaces + toPrint + spaces;

        return toPrint;

    }

    public List<String> getPics() {
        return pics;
    }

}
