package net.smallbulletin.recursionapp;

public class SearchInput {
    private String[] array;
    private String searchFor;

    public String[] getArray() {
        return this.array;
    }

    public void setArray(final String[] array) {
        this.array = array;
    }

    public String getSearchFor() {
        return this.searchFor;
    }

    public void setSearchFor(final String searchFor) {
        this.searchFor = searchFor;
    }

}
