package net.smallbulletin.recursionapp;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Output<E> {
    private final E output;

    @JsonCreator
    public Output(@JsonProperty("output") final E output) {
        this.output = output;
    }

    public E getOutput() {
        return this.output;
    }

}
