package net.smallbulletin.recursionapp;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class HanoiInput {
    private int a;

    @JsonCreator
    public HanoiInput(@JsonProperty("a") int a) {
        this.a = a;
    }

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

}
