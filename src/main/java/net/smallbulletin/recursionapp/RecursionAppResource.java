package net.smallbulletin.recursionapp;

import java.util.List;
import java.util.Set;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.codahale.metrics.annotation.Timed;

import net.smallbulletin.recursionapp.util.Anagram;
import net.smallbulletin.recursionapp.util.BinarySearch;
import net.smallbulletin.recursionapp.util.Factorial;
import net.smallbulletin.recursionapp.util.Hanoi;
import net.smallbulletin.recursionapp.util.TriangularNumbers;

@Path("/recursionapp")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class RecursionAppResource {
    @POST
    @Timed
    @Path("/factorial")
    public Output<Long> factorial(final Input<Integer> input) {
        final long num = Factorial.factorial(input.getInput());

        return new Output<Long>(num);
    }

    @POST
    @Timed
    @Path("/triangular")
    public Output<Long> triangular(final Input<Integer> input) {

        final long num = TriangularNumbers.getTriangular((input.getInput()));

        return new Output<Long>(num);
    }

    @POST
    @Timed
    @Path("/anagram")
    public Output<Set<String>> anagram(final Input<String> input) {

        return new Output<>(Anagram.anagram(input.getInput()));
    }

    @POST
    @Timed
    @Path("/search")
    public SearchOutput search(final SearchInput input) {
        final int index = BinarySearch.search(input.getArray(), input.getSearchFor(), 0, input.getArray().length - 1);

        final SearchOutput output = new SearchOutput(index, input.getArray());

        return output;
    }

    @POST
    @Timed
    @Path("/hanoi")
    public HanoiOutput hanoi(HanoiInput input) {
        Hanoi hanoi = new Hanoi(input.getA());
        hanoi.run();
        List<String> pics = hanoi.getPics();
        HanoiOutput output = new HanoiOutput(pics);

        return output;
    }
}
