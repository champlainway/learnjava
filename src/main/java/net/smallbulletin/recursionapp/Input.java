package net.smallbulletin.recursionapp;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Input<E> {
    private final E input;

    @JsonCreator
    public Input(@JsonProperty("input") final E input) {
        this.input = input;
    }

    public E getInput() {
        return this.input;
    }

}
