package net.smallbulletin.bintree.huffman;

import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;
import java.util.TreeMap;

public class HuffmanEncryption {
    private final String toEncode;
    private final Map<Character, String> codeTable = new HashMap<Character, String>();
    private HuffmanTree decodeTree;

    public HuffmanEncryption(final String toEncode) {
        this.toEncode = toEncode;
        genTree();
        genCodeTable();
    }

    public String encrypt() {
        String encoded = "";

        for (int i = 0; i < toEncode.length(); i++) {
            final char curr = toEncode.charAt(i);

            encoded += codeTable.get(curr);
        }

        return encoded;
    }

    public String decrypt(final String encrypted) {
        String result = "";
        HuffmanTreeNode sub = decodeTree.getRoot();

        for (int i = 0; i < encrypted.length(); i++) {
            final char curr = encrypted.charAt(i);

            if (curr == '0') {
                sub = sub.getLeft();
            } else {
                sub = sub.getRight();
            }

            if (sub.getValue() != null) {
                result += sub.getValue();
                sub = decodeTree.getRoot();
            }
        }

        return result;
    }

    private void genTree() {
        final Queue<HuffmanTree> forest = new PriorityQueue<>();
        final Map<Character, Integer> frequencies = new TreeMap<Character, Integer>();

        // get the frequency of each character
        for (int i = 0; i < toEncode.length(); i++) {
            final char curr = toEncode.charAt(i);
            if (frequencies.containsKey(curr)) {
                frequencies.put(curr, frequencies.get(curr) + 1);
            } else {
                frequencies.put(curr, 1);
            }
        }

        // put them into the queue as individual trees
        final Set<Character> keys = frequencies.keySet();

        for (final Character c : keys) {
            final HuffmanTreeNode curr = new HuffmanTreeNode(c, frequencies.get(c));
            final HuffmanTree tmp = new HuffmanTree(curr);
            forest.add(tmp);
        }

        while (forest.size() > 1) {
            final HuffmanTree tmpOne = forest.poll();
            final HuffmanTree tmpTwo = forest.poll();
            final HuffmanTreeNode newNode = new HuffmanTreeNode(
                    tmpOne.getRoot().getFrequency() + tmpTwo.getRoot().getFrequency());
            newNode.setLeft(tmpOne.getRoot());
            newNode.setRight(tmpTwo.getRoot());
            final HuffmanTree newTree = new HuffmanTree(newNode);

            forest.add(newTree);
        }

        if (forest.size() == 1) {
            decodeTree = forest.poll();
            final HuffmanTreeNode root = decodeTree.getRoot();
            if (root.isLeaf()) {
                final HuffmanTreeNode newNode = new HuffmanTreeNode(root.getFrequency());
                newNode.setLeft(root);
                final HuffmanTree newTree = new HuffmanTree(newNode);
                decodeTree = newTree;
            }
        } else if (forest.size() == 0) {
            decodeTree = new HuffmanTree(new HuffmanTreeNode(0));
        }
    }

    private void genCodeTable() {
        for (int i = 0; i < toEncode.length(); i++) {
            final char curr = toEncode.charAt(i);

            getCode(curr);
        }
    }

    private void getCode(final char c) {
        final String code = getCode(decodeTree.getRoot(), c);

        codeTable.put(c, code);
    }

    private String getCode(final HuffmanTreeNode sub, final char c) {
        if (sub == null) {
            return null;
        }

        if (sub.getValue() != null && sub.getValue() == c) {
            return "";
        }

        final String left = getCode(sub.getLeft(), c);
        final String right = getCode(sub.getRight(), c);

        if (left == null && right == null) {
            return null;
        } else if (left == null) {
            return "1" + right;
        } else {
            return "0" + left;
        }

    }

    HuffmanTree getDecodeTree() {
        return decodeTree;
    }

    Map<Character, String> getCodeTable() {
        return codeTable;
    }

}
