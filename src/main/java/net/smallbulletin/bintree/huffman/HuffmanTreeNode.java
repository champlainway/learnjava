package net.smallbulletin.bintree.huffman;

public class HuffmanTreeNode {
    private Character value = null;
    private int frequency = 0;
    private HuffmanTreeNode right = null;
    private HuffmanTreeNode left = null;

    public HuffmanTreeNode(final int frequency) {
        this.frequency = frequency;
    }

    public HuffmanTreeNode(final char value, final int frequency) {
        this.value = value;
        this.frequency = frequency;
    }

    public Character getValue() {
        return value;
    }

    public int getFrequency() {
        return frequency;
    }

    void setRight(final HuffmanTreeNode right) {
        this.right = right;
    }

    void setLeft(final HuffmanTreeNode left) {
        this.left = left;
    }

    public HuffmanTreeNode getRight() {
        return right;
    }

    public HuffmanTreeNode getLeft() {
        return left;
    }

    public boolean isLeaf() {
        return right == null && left == null;
    }

}
