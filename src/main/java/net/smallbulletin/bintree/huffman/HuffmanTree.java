package net.smallbulletin.bintree.huffman;

public class HuffmanTree implements Comparable<HuffmanTree> {
    private HuffmanTreeNode root = null;

    public HuffmanTree(final HuffmanTreeNode root) {
        this.root = root;

    }

    HuffmanTreeNode getRoot() {
        return root;
    }

    @Override
    public int compareTo(final HuffmanTree o) {
        final Integer self = root.getFrequency();
        final Integer notSelf = o.getRoot().getFrequency();

        return self.compareTo(notSelf);
    }

}
