package net.smallbulletin.bintree.redblack;

public class RBTreeNode<E extends Comparable<E>> {
    private final E value;
    private boolean isRed = true; // if not red, then black
    private RBTreeNode<E> left = null;
    private RBTreeNode<E> right = null;
    private RBTreeNode<E> parent = null;

    public RBTreeNode() {
        this.value = null;
        this.isRed = false;
    }

    public RBTreeNode(final E value) {
        this.value = value;
        this.left = new RBTreeNode<E>();
        left.setParent(this);
        this.right = new RBTreeNode<E>();
        right.setParent(this);
    }

    void setRed(final boolean red) {
        this.isRed = red;
    }

    void flipColor() {
        isRed = !isRed;
    }

    void setRight(final RBTreeNode<E> right) {
        this.right = right;
    }

    void setLeft(final RBTreeNode<E> left) {
        this.left = left;
    }

    void setRightNull() {
        final RBTreeNode<E> nullChild = new RBTreeNode<>();
        nullChild.setParent(this);
        right = nullChild;
    }

    void setLeftNull() {
        final RBTreeNode<E> nullChild = new RBTreeNode<>();
        nullChild.setParent(this);
        left = nullChild;
    }

    public RBTreeNode<E> getRight() {
        return right;
    }

    public RBTreeNode<E> getLeft() {
        return left;
    }

    public E getValue() {
        return value;
    }

    public boolean hasLeft() {
        return !RBTreeNode.isNull(left);
    }

    public boolean hasRight() {
        return !RBTreeNode.isNull(right);
    }

    public boolean isLeaf() {
        return !hasLeft() && !hasRight();
    }

    public boolean isRed() {
        return isRed;
    }

    public boolean isNullChild() {
        return value == null;
    }

    void setParent(final RBTreeNode<E> parent) {
        this.parent = parent;
    }

    public RBTreeNode<E> getParent() {
        return parent;
    }

    public RBTreeNode<E> getGrandparent() {
        if (parent == null) {
            return null;
        }

        return parent.getParent();
    }

    public RBTreeNode<E> getUncle() {
        final RBTreeNode<E> grandparent = getGrandparent();

        if (grandparent == null) {
            return null;
        }

        if (grandparent.getLeft() == parent) {
            return grandparent.getRight();
        }

        return grandparent.getLeft();
    }

    public RBTreeNode<E> getSibling() {
        if (parent == null) {
            return null; // root has null parent, and no siblings
        }

        if (parent.getLeft() == this) {
            return parent.getRight();
        }

        return parent.getLeft();
    }

    @Override
    public String toString() {
        final String result = "(" + value + ", " + ((isRed) ? "R" : "B") + ")";
        return result;
    }

    public static <E extends Comparable<E>> RBTreeNode<E> copyOf(final RBTreeNode<E> node) {
        final RBTreeNode<E> newNode = new RBTreeNode<E>(node.getValue());
        newNode.setRed(node.isRed());

        return newNode;
    }

    public static <E extends Comparable<E>> boolean isNull(final RBTreeNode<E> node) {
        return node == null || node.getValue() == null;
    }

}
