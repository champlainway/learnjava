package net.smallbulletin.bintree.redblack;

import java.util.LinkedList;
import java.util.Queue;

import com.google.common.base.Preconditions;

import net.smallbulletin.bintree.TreeNode;

public class RBTree<E extends Comparable<E>> {
    private RBTreeNode<E> root = null;

    public RBTree() {

    }

    public RBTree(final RBTreeNode<E> root) {
        this.root = root;
    }

    RBTreeNode<E> getRoot() {
        return root;
    }

    public RBTreeNode<E> find(final E key) {
        return find(root, key);
    }

    private RBTreeNode<E> find(final RBTreeNode<E> sub, final E key) {
        if (RBTreeNode.isNull(sub)) {
            return null;
        }
        final E currVal = sub.getValue();
        final int compare = key.compareTo(currVal);

        if (compare == 0) {
            return sub;
        } else if (compare < 0) {
            if (RBTreeNode.isNull(sub.getLeft())) {
                return null;
            }

            return find(sub.getLeft(), key);
        } else {
            if (RBTreeNode.isNull(sub.getRight())) {
                return null;
            }

            return find(sub.getRight(), key);
        }
    }

    public void insert(final E value) {
        final RBTreeNode<E> toInsert = insert(root, value);
        if (toInsert == null) {
            return;
        }

        insertCaseOne(toInsert);
    }

    private RBTreeNode<E> insert(final RBTreeNode<E> sub, final E value) {
        if (root == null) {
            final RBTreeNode<E> toInsert = new RBTreeNode<>(value);
            root = toInsert;
            return toInsert;
        }

        final int compare = value.compareTo(sub.getValue());

        if (compare == 0) {
            return null;
        } else if (compare > 0) {

            if (sub.hasRight()) {
                return insert(sub.getRight(), value);
            } else {
                final RBTreeNode<E> toInsert = new RBTreeNode<>(value);
                sub.setRight(toInsert);
                toInsert.setParent(sub);
                return toInsert;
            }
        } else {
            if (sub.hasLeft()) {
                return insert(sub.getLeft(), value);
            } else {
                final RBTreeNode<E> toInsert = new RBTreeNode<>(value);
                sub.setLeft(toInsert);
                toInsert.setParent(sub);
                return toInsert;
            }
        }
    }

    private void rotateLeft(final RBTreeNode<E> sub) {
        final RBTreeNode<E> pivot = sub.getRight();
        sub.setRight(pivot.getLeft());
        pivot.setLeft(sub);

        if (root == sub) {
            root = pivot;
            pivot.setParent(null);
            sub.setParent(pivot);
        } else {
            final RBTreeNode<E> parent = sub.getParent();

            if (parent.getLeft() == sub) {
                parent.setLeft(pivot);
            } else {
                parent.setRight(pivot);
            }

            pivot.setParent(parent);
            sub.setParent(pivot);
        }
    }

    private void rotateRight(final RBTreeNode<E> sub) {
        final RBTreeNode<E> pivot = sub.getLeft();
        sub.setLeft(pivot.getRight());
        pivot.setRight(sub);

        if (root == sub) {
            root = pivot;
            sub.setParent(pivot);
            pivot.setParent(null);
        } else {
            final RBTreeNode<E> parent = sub.getParent();

            if (parent.getLeft() == sub) {
                parent.setLeft(pivot);

            } else {
                parent.setRight(pivot);
            }

            pivot.setParent(parent);
            sub.setParent(pivot);
        }
    }

    // START DIFFERENT BALANCING CASES FOR INSERT
    private void insertCaseOne(final RBTreeNode<E> toInsert) {
        if (toInsert == root) {
            toInsert.setRed(false);
        } else {
            insertCaseTwo(toInsert);
        }
    }

    private void insertCaseTwo(final RBTreeNode<E> toInsert) {
        final RBTreeNode<E> parent = toInsert.getParent();

        if (!parent.isRed()) {
            return;
        } else {
            insertCaseThree(toInsert);
        }
    }

    private void insertCaseThree(final RBTreeNode<E> toInsert) {
        final RBTreeNode<E> parent = toInsert.getParent();
        final RBTreeNode<E> uncle = toInsert.getUncle();
        final RBTreeNode<E> grandparent = toInsert.getGrandparent();

        if (uncle != null && uncle.isRed()) {
            parent.setRed(false);
            uncle.setRed(false);
            grandparent.setRed(true);
            insertCaseOne(grandparent);
        } else {
            insertCaseFour(toInsert);
        }
    }

    private void insertCaseFour(RBTreeNode<E> toInsert) {
        final RBTreeNode<E> parent = toInsert.getParent();
        final RBTreeNode<E> grandparent = toInsert.getGrandparent();

        if (toInsert == parent.getRight() && grandparent.getLeft() == parent) {
            rotateLeft(parent);
            toInsert = toInsert.getLeft();
        } else if (grandparent.getRight() == parent && toInsert == parent.getLeft()) {
            rotateRight(parent);

            toInsert = toInsert.getRight();
        }

        insertCaseFive(toInsert);
    }

    private void insertCaseFive(final RBTreeNode<E> toInsert) {
        final RBTreeNode<E> parent = toInsert.getParent();
        final RBTreeNode<E> grandparent = toInsert.getGrandparent();

        parent.setRed(false);
        grandparent.setRed(true);

        if (toInsert == parent.getRight() && grandparent.getRight() == parent) {
            rotateLeft(grandparent);
        } else if (toInsert == parent.getLeft() && grandparent.getLeft() == parent) {
            rotateRight(grandparent);
        }
    }
    // END BALANCING CASES FOR INSERT

    // Traversals
    public String inOrder() {
        return inOrder(root);
    }

    private String inOrder(final RBTreeNode<E> sub) {
        String color = "";

        if (RBTreeNode.isNull(sub)) {
            return "";
        }

        if (sub.isRed()) {
            color = "R";
        } else {
            color = "B";
        }

        return inOrder(sub.getLeft()) + "(" + sub.getValue() + ", " + color + ")" + inOrder(sub.getRight());
    }

    public String preOrder() {
        return preOrder(root);
    }

    private String preOrder(final RBTreeNode<E> sub) {
        String color = "";

        if (RBTreeNode.isNull(sub)) {
            return "";
        }

        if (sub.isRed()) {
            color = "R";
        } else {
            color = "B";
        }

        return "(" + sub.getValue() + ", " + color + ")" + preOrder(sub.getLeft()) + preOrder(sub.getRight());
    }

    public String postOrder() {
        return postOrder(root);
    }

    private String postOrder(final RBTreeNode<E> sub) {
        String color = "";

        if (RBTreeNode.isNull(sub)) {
            return "";
        }

        if (sub.isRed()) {
            color = "R";
        } else {
            color = "B";
        }

        return postOrder(sub.getLeft()) + postOrder(sub.getRight()) + "(" + sub.getValue() + ", " + color + ")";
    }

    public String reverseOrder() {
        return reverseOrder(root);
    }

    private String reverseOrder(final RBTreeNode<E> sub) {
        String color = "";

        if (RBTreeNode.isNull(sub)) {
            return "";
        }

        if (sub.isRed()) {
            color = "R";
        } else {
            color = "B";
        }

        return reverseOrder(sub.getRight()) + "(" + sub.getValue() + ", " + color + ")" + postOrder(sub.getLeft());
    }

    public String level() {
        final Queue<RBTreeNode<E>> nodes = new LinkedList<RBTreeNode<E>>();
        String result = "";
        String color = "";
        nodes.add(root);

        while (!nodes.isEmpty()) {
            final RBTreeNode<E> tmp = nodes.remove();

            if (tmp == null) {
                result += "";
                continue;
            }

            if (tmp.isRed()) {
                color = "R";
            } else {
                color = "B";
            }

            result += "(" + tmp.getValue() + ", " + color + ")";
            if (!tmp.isLeaf()) {
                nodes.add(tmp.getLeft());
                nodes.add(tmp.getRight());
            }
        }

        return result;
    }
    // End traversals

    // Begin delete
    public void delete(final E key) {
        final RBTreeNode<E> curr = find(key);
        final RBTreeNode<E> parent = curr.getParent();

        boolean onLeft = false;

        if (curr != root) {
            if (parent.getLeft() == curr) {
                onLeft = true;
            } else {
                onLeft = false;
            }
        }

        if (curr.isLeaf()) {
            if (curr == root) {
                root = null;
            } else if (onLeft) {
                parent.setLeftNull();

            } else {
                parent.setRightNull();

            }

            if (root != null && !curr.isRed()) {
                if (onLeft) {
                    deleteCaseOne(parent.getLeft());
                } else {
                    deleteCaseOne(parent.getRight());
                }

            }
        } else if (!curr.hasRight()) {
            final RBTreeNode<E> child = curr.getLeft();

            child.setParent(parent);

            if (curr == root) {
                root = child;
            } else if (onLeft) {
                parent.setLeft(child);
            } else {
                parent.setRight(child);
            }

            if (!curr.isRed()) {
                child.setRed(false);
            }

        } else if (!curr.hasLeft()) {
            final RBTreeNode<E> child = curr.getRight();

            child.setParent(parent);

            if (curr == root) {
                root = child;
            } else if (onLeft) {
                parent.setLeft(child);
            } else {
                parent.setRight(child);
            }

            if (!curr.isRed()) {
                child.setRed(false);
            }
        } else {

            final RBTreeNode<E> newNodeParent = getSuccessorParent(curr);
            RBTreeNode<E> newNode = newNodeParent.getLeft();

            if (newNode.getValue() == null) {
                newNode = newNodeParent;
            } else {
                newNodeParent.setLeftNull();
            }

            if (curr.getRight() == newNode) {

                curr.getRight().getRight().setParent(newNode);

            } else {
                newNode.setRight(curr.getRight());
                curr.getRight().setParent(newNode);
            }

            newNode.setLeft(curr.getLeft());
            curr.getLeft().setParent(newNode);
            newNode.setParent(parent);
            if (parent != null) {
                if (onLeft) {
                    parent.setLeft(newNode);
                } else {
                    parent.setRight(newNode);
                }
            }

            if (curr == root) {
                root = newNode;
            }

            if (!curr.isRed()) {
                if (newNode.isRed()) {
                    newNode.setRed(false);
                } else {
                    deleteCaseOne(newNode);
                }
            }
        }
        // }
    }

    private RBTreeNode<E> getSuccessorParent(final RBTreeNode<E> sub) {
        RBTreeNode<E> tmp = sub.getRight();
        RBTreeNode<E> parent = tmp;

        while (tmp.getLeft().getValue() != null) {
            parent = tmp;
            tmp = tmp.getLeft();
        }

        return parent;
    }

    private void deleteCaseOne(final RBTreeNode<E> toDelete) {

        Preconditions.checkArgument(toDelete.isRed() == false, "Node to balance should be black");

        if (toDelete != root) {
            deleteCaseTwo(toDelete);
        }
    }

    private void deleteCaseTwo(final RBTreeNode<E> toDelete) {
        final RBTreeNode<E> parent = toDelete.getParent();
        final RBTreeNode<E> sibling = toDelete.getSibling();

        if (!RBTreeNode.isNull(sibling) && sibling.isRed()) {
            parent.setRed(true);
            sibling.setRed(false);

            if (toDelete == parent.getLeft()) {
                rotateLeft(parent);
            } else {
                rotateRight(parent);
            }
        }

        deleteCaseThree(toDelete);
    }

    private void deleteCaseThree(final RBTreeNode<E> toDelete) {
        final RBTreeNode<E> parent = toDelete.getParent();
        final RBTreeNode<E> sibling = toDelete.getSibling();

        if (RBTreeNode.isNull(sibling)) {
            return;
        }

        if (!parent.isRed() && !sibling.isRed() && !sibling.getLeft().isRed() && !sibling.getRight().isRed()) {
            sibling.setRed(true);
            deleteCaseOne(parent);
        } else {
            deleteCaseFour(toDelete);
        }
    }

    private void deleteCaseFour(final RBTreeNode<E> toDelete) {
        final RBTreeNode<E> parent = toDelete.getParent();
        final RBTreeNode<E> sibling = toDelete.getParent();

        if (RBTreeNode.isNull(sibling)) {
            return;
        }

        if (parent.isRed() && !sibling.isRed() && !sibling.getLeft().isRed() && !sibling.getRight().isRed()) {
            sibling.setRed(true);
            parent.setRed(false);
        } else {
            deleteCaseFive(toDelete);
        }
    }

    private void deleteCaseFive(final RBTreeNode<E> toDelete) {
        final RBTreeNode<E> parent = toDelete.getParent();
        final RBTreeNode<E> sibling = toDelete.getSibling();

        if (!RBTreeNode.isNull(sibling) && !sibling.isRed()) {
            if (sibling.getLeft().isRed() && !sibling.getRight().isRed() && toDelete == parent.getLeft()) {
                sibling.setRed(true);
                sibling.getLeft().setRed(false);
                rotateRight(sibling);
            } else if (sibling.getRight().isRed() && !sibling.getLeft().isRed() && toDelete == parent.getRight()) {
                sibling.setRed(true);
                sibling.getRight().setRed(false);
                rotateLeft(sibling);

            }
        }

        deleteCaseSix(toDelete);
    }

    public void deleteCaseSix(final RBTreeNode<E> toDelete) {
        final RBTreeNode<E> parent = toDelete.getParent();
        final RBTreeNode<E> sibling = toDelete.getSibling();

        sibling.setRed(parent.isRed());
        parent.setRed(false);
        if (RBTreeNode.isNull(sibling)) {
            return;
        }

        if (toDelete == parent.getLeft()) {
            sibling.getRight().setRed(false);
            rotateLeft(parent);
        } else if (toDelete == parent.getRight()) {
            sibling.getLeft().setRed(false);
            rotateRight(parent);
        }
    }
    // End delete

    // Helpers

}
