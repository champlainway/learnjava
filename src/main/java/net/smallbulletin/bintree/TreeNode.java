package net.smallbulletin.bintree;

public class TreeNode<E extends Comparable<E>> {
    private E val;
    private TreeNode<E> left = null;
    private TreeNode<E> right = null;

    TreeNode(final E val) {
        this.val = val;
    }

    TreeNode(final E val, final TreeNode<E> left, final TreeNode<E> right) {
        this.val = val;
        this.left = left;
        this.right = right;
    }

    public E getValue() {
        return val;
    }

    void setValue(final E val) {
        this.val = val;
    }

    public TreeNode<E> getLeft() {
        return left;
    }

    public TreeNode<E> getRight() {
        return right;
    }

    void setLeft(final TreeNode<E> left) {
        this.left = left;
    }

    void setRight(final TreeNode<E> right) {
        this.right = right;
    }

    public boolean hasLeft() {
        return left != null;
    }

    public boolean hasRight() {
        return right != null;
    }

    public boolean isLeaf() {
        return !hasLeft() && !hasRight();
    }

}
