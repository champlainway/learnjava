package net.smallbulletin.huffman.util;

import java.util.Stack;

public class HuffmanTree implements Comparable<HuffmanTree> {
    private HuffmanTreeNode root = null;

    public HuffmanTree(final HuffmanTreeNode root) {
        this.root = root;

    }

    HuffmanTreeNode getRoot() {
        return root;
    }

    @Override
    public int compareTo(final HuffmanTree o) {
        final Integer self = root.getFrequency();
        final Integer notSelf = o.getRoot().getFrequency();

        return self.compareTo(notSelf);
    }

    public String serialize() {
        return postOrder(root);
    }

    private String postOrder(final HuffmanTreeNode sub) {
        if (sub == null) {
            return "^";
        }

        final Character val = sub.getValue();

        return "(" + postOrder(sub.getLeft()) + postOrder(sub.getRight()) + (val == null ? "#" : val) + ")";
    }

    public static HuffmanTree deserialize(final String serialized) {
        final Stack<HuffmanTreeNode> nodes = new Stack<>();

        for (int i = 0; i < serialized.length(); i++) {
            final char curr = serialized.charAt(i);

            onChar(curr, nodes);
        }

        final HuffmanTree tree = new HuffmanTree(nodes.pop());

        if (nodes.isEmpty()) {
            return tree;
        }

        throw new IllegalArgumentException("Not a complete serialized tree: " + serialized);
    }

    private static void onChar(final char c, final Stack<HuffmanTreeNode> nodes) {
        if (c == '^') {
            nodes.push(null);
        } else if (c == '#') {
            final HuffmanTreeNode tmp = new HuffmanTreeNode();
            nodes.push(tmp);
        } else if (c == ')') {
            final HuffmanTreeNode tmp = new HuffmanTreeNode(c);
            nodes.push(tmp);

            while (collapseIfPossible(nodes)) {

            }

        } else if (Character.isLetterOrDigit(c) || c == '(') {
            final HuffmanTreeNode tmp = new HuffmanTreeNode(c);
            nodes.push(tmp);
        } else {
            throw new IllegalArgumentException("Invalid character: " + c);
        }
    }

    private static boolean collapseIfPossible(final Stack<HuffmanTreeNode> nodes) {
        if (nodes.size() < 5) {
            return false;
        }

        final int size = nodes.size();

        final HuffmanTreeNode closing = nodes.peek(); // closing parentheses
        final HuffmanTreeNode root = nodes.get(size - 2);
        final HuffmanTreeNode right = nodes.get(size - 3);
        final HuffmanTreeNode left = nodes.get(size - 4);
        final HuffmanTreeNode opening = nodes.get(size - 5);

        if (closing.getValue() != ')' || opening.getValue() != '(') {
            return false;
        } else {
            root.setLeft(left);
            root.setRight(right);

            nodes.pop();
            nodes.pop();
            nodes.pop();
            nodes.pop();
            nodes.pop();

            nodes.push(root);

            return true;
        }

    }

}
