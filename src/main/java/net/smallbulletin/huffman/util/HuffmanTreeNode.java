package net.smallbulletin.huffman.util;

public class HuffmanTreeNode {
    private Character value = null;
    private int frequency = 0;
    private HuffmanTreeNode right = null;
    private HuffmanTreeNode left = null;

    // placeholder node
    public HuffmanTreeNode() {

    }

    public HuffmanTreeNode(final char value) {
        this.value = value;
    }

    public HuffmanTreeNode(final int frequency) {
        this.frequency = frequency;
    }

    public HuffmanTreeNode(final char value, final int frequency) {
        this.value = value;
        this.frequency = frequency;
    }

    public Character getValue() {
        return value;
    }

    public int getFrequency() {
        return frequency;
    }

    void setRight(final HuffmanTreeNode right) {
        this.right = right;
    }

    void setLeft(final HuffmanTreeNode left) {
        this.left = left;
    }

    public HuffmanTreeNode getRight() {
        return right;
    }

    public HuffmanTreeNode getLeft() {
        return left;
    }

    public boolean isLeaf() {
        return hasLeft() && hasRight();
    }

    public boolean hasLeft() {
        return left != null;
    }

    public boolean hasRight() {
        return right != null;
    }

    @Override
    public String toString() {
        return (value == null ? "^" : value) + "" + frequency;
    }

}
