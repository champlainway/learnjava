package net.smallbulletin.huffman;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Output {
    private final String str;

    @JsonCreator
    public Output(@JsonProperty("str") final String str) {
        this.str = str;
    }

    public String getStr() {
        return str;
    }
}
