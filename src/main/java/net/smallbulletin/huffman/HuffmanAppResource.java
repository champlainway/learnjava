package net.smallbulletin.huffman;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.codahale.metrics.annotation.Timed;

import net.smallbulletin.huffman.util.HuffmanEncryption;
import net.smallbulletin.huffman.util.HuffmanTree;

@Path("/huffman")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class HuffmanAppResource {

    @POST
    @Timed
    @Path("/encrypt")
    public Output encrypt(final Input input) {
        final HuffmanEncryption encryption = new HuffmanEncryption(input.getStr());
        final String encrypted = encryption.encrypt();
        final String tree = encryption.getDecodeTree().serialize();
        final Output output = new Output(tree + "|" + encrypted);

        return output;
    }

    @POST
    @Timed
    @Path("/decrypt")
    public Output decrypt(final Input input) {
        final String str = input.getStr();
        final String serialized = str.substring(0, str.indexOf('|'));
        final String encrypted = str.substring(str.indexOf('|') + 1);
        final HuffmanTree decodeTree = HuffmanTree.deserialize(serialized);
        final String decrypted = HuffmanEncryption.decrypt(encrypted, decodeTree);
        final Output output = new Output(decrypted);

        return output;
    }

}
