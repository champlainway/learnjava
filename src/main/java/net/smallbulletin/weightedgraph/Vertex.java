package net.smallbulletin.weightedgraph;

public class Vertex<E extends Comparable<E>> implements Comparable<Vertex<E>> {
    private E value;
    private boolean visited;

    public Vertex() {

    }

    public Vertex(final E value) {
        this.value = value;
    }

    public E getValue() {
        return value;
    }

    public boolean isVisited() {
        return visited;
    }

    public void setVisited(final boolean visited) {
        this.visited = visited;
    }

    @Override
    public String toString() {
        return value + "";
    }

    @Override
    public int compareTo(final Vertex<E> o) {
        return value.compareTo(o.getValue());
    }
}
