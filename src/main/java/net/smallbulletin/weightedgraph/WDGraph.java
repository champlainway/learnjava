package net.smallbulletin.weightedgraph;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import net.smallbulletin.graph.util.Vertex;

public class WDGraph<E extends Comparable<E>> extends WeightedGraph<E> {

    private final List<Vertex<E>> graph;
    private int[][] adj;
    private DistanceToParent[] paths;
    private final int inf = 10000000;

    public WDGraph() {
        graph = new ArrayList<Vertex<E>>();
        adj = new int[0][0];
        paths = new DistanceToParent[0];
    }

    @Override
    public void addVertex(final E newVal) {
        if (!containsValue(newVal)) {
            final Vertex<E> newVertex = new Vertex<>(newVal);

            graph.add(newVertex);
            adj = expandMatrix();
            paths = expandPaths();
        }
    }

    @Override
    public void addEdgeIndex(final int a, final int b, final int weight) {
        if (a < 0 || a >= graph.size()) {
            throw new IndexOutOfBoundsException();
        }

        if (b < 0 || b >= graph.size()) {
            throw new IndexOutOfBoundsException();
        }

        adj[a][b] = weight;
    }

    @Override
    public void addEdgeValue(final E a, final E b, final int weight) {
        if (!containsValue(a)) {
            throw new NoSuchElementException();
        }

        if (!containsValue(b)) {
            throw new NoSuchElementException();
        }

        final int indA = vertexWithValue(a);
        final int indB = vertexWithValue(b);

        addEdgeIndex(indA, indB, weight);
    }

    public String dijkstra() {
        int currVertex = 0;
        int startToCurrent = 0;
        int treeLength = 0;
        final int startIndex = 0;
        graph.get(startIndex).setVisited(true);

        for (int i = 0; i < paths.length; i++) {
            final int tmp = adj[startIndex][i];
            paths[i] = new DistanceToParent(startIndex, tmp);
        }

        while (treeLength < graph.size()) {
            final int minInd = getMin();
            final int minDist = paths[minInd].getDistance();

            if (minDist == inf) {
                break;
            } else {
                currVertex = minInd;
                startToCurrent = paths[minInd].getDistance();
            }

            graph.get(currVertex).setVisited(true);
            treeLength++;
            adjustPaths(currVertex, startToCurrent);
        }

        final String result = getLengths();
        resetVisit();

        return result;

    }

    private int[][] expandMatrix() {
        final int newSize = adj.length + 1;
        final int[][] toReturn = new int[newSize][newSize];

        for (int i = 0; i < toReturn.length; i++) {
            for (int j = 0; j < toReturn.length; j++) {
                toReturn[i][j] = inf;
            }
        }

        for (int i = 0; i < adj.length; i++) {
            for (int j = 0; j < adj.length; j++) {
                toReturn[i][j] = adj[i][j];
            }
        }

        return toReturn;
    }

    private DistanceToParent[] expandPaths() {
        final int newSize = paths.length + 1;
        final DistanceToParent[] toReturn = new DistanceToParent[newSize];

        for (int i = 0; i < paths.length; i++) {
            toReturn[i] = paths[i];
        }

        return toReturn;
    }

    private int vertexWithValue(final E e) {

        for (int i = 0; i < graph.size(); i++) {
            final Vertex<E> tmp = graph.get(i);

            if (tmp.getValue().equals(e)) {
                return i;
            }
        }

        return -1;
    }

    private int getMin() {
        int min = inf;
        int minIndex = 0;

        for (int i = 1; i < graph.size(); i++) {
            if (!graph.get(i).isVisited() && paths[i].getDistance() < min) {
                min = paths[i].getDistance();
                minIndex = i;
            }
        }

        return minIndex;
    }

    private void adjustPaths(final int currVertex, final int startToCurr) {
        int col = 1;

        while (col < graph.size()) {
            if (graph.get(col).isVisited()) {
                col++;
                continue;
            }

            // fringe- node connected to a visited one, but not yet visited

            final int currToFringe = adj[currVertex][col];
            final int startToFringe = startToCurr + currToFringe;

            final int pathLength = paths[col].getDistance();

            if (startToFringe < pathLength) {
                paths[col].setParentInd(currVertex);
                paths[col].setDistance(startToFringe);
            }

            col++;
        }
    }

    private String getLengths() {
        String result = "";

        for (int i = 0; i < graph.size(); i++) {
            result += graph.get(i).getValue() + "=";

            if (paths[i].getDistance() == inf) {
                result += "INF";

            } else {
                result += paths[i].getDistance();
            }

            final E parent = graph.get(paths[i].getParentInd()).getValue();
            result += "(from " + parent + ") ";
        }

        return result.trim();
    }

}
