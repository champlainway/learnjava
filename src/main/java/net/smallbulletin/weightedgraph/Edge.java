package net.smallbulletin.weightedgraph;

public class Edge implements Comparable<Edge> {
    private final int fromIndex;
    private final int toIndex;
    private final int distance;

    public Edge(final int fromIndex, final int toIndex, final int distance) {
        this.fromIndex = fromIndex;
        this.toIndex = toIndex;
        this.distance = distance;
    }

    public int getDistance() {
        return distance;
    }

    public int getFromIndex() {
        return fromIndex;
    }

    public int getToIndex() {
        return toIndex;
    }

    @Override
    public int compareTo(final Edge arg0) {
        return Integer.compare(distance, arg0.distance);
    }
}
