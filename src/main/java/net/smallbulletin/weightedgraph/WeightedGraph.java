package net.smallbulletin.weightedgraph;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;

import net.smallbulletin.graph.util.Vertex;

public class WeightedGraph<E extends Comparable<E>> {
    private final List<Vertex<E>> graph;
    private int[][] adj;
    private final EdgeQueue priority;
    private String mst;

    public WeightedGraph() {
        graph = new ArrayList<Vertex<E>>();
        adj = new int[0][0];
        priority = new EdgeQueue();
        mst = "";
    }

    public void addVertex(final E newVal) {
        if (!containsValue(newVal)) {
            final Vertex<E> newVertex = new Vertex<>(newVal);

            graph.add(newVertex);
            adj = expandMatrix();
        }
    }

    public void addEdgeIndex(final int a, final int b, final int weight) {
        if (a < 0 || a >= graph.size()) {
            throw new IndexOutOfBoundsException();
        }

        if (b < 0 || b >= graph.size()) {
            throw new IndexOutOfBoundsException();
        }

        adj[a][b] = weight;
        adj[b][a] = weight;
    }

    public void addEdgeValue(final E a, final E b, final int weight) {
        if (!containsValue(a)) {
            throw new NoSuchElementException();
        }

        if (!containsValue(b)) {
            throw new NoSuchElementException();
        }

        final int indA = vertexWithValue(a);
        final int indB = vertexWithValue(b);

        addEdgeIndex(indA, indB, weight);
    }

    public String getMST() {
        minimalTree();
        return mst.trim();
    }

    private void minimalTree() {

        int currIndex = 0;

        while (mst.length() < (graph.size() - 1) * 4) {
            final Vertex<E> curr = graph.get(currIndex);
            curr.setVisited(true);

            for (int i = 0; i < graph.size(); i++) {
                if (currIndex == i) {
                    continue;
                }
                final Vertex<E> tmp = graph.get(i);

                if (tmp.isVisited()) {
                    continue;
                }

                final int distance = adj[currIndex][i];

                if (distance == 0) {
                    continue;
                }

                priorityInsert(currIndex, i, distance);
            }

            if (priority.size() == 0) {
                break;
            }

            final Edge currEdge = priority.remove();
            final int from = currEdge.getFromIndex();
            currIndex = currEdge.getToIndex();

            final E fromVal = graph.get(from).getValue();
            final E toVal = graph.get(currIndex).getValue();

            mst += fromVal + "-" + toVal + " ";
        }

        if (mst.length() < (graph.size() - 1) * 4) {
            mst = "";
        }

        resetVisit();

    }

    private int[][] expandMatrix() {
        final int newSize = adj.length + 1;
        final int[][] toReturn = new int[newSize][newSize];

        for (int i = 0; i < adj.length; i++) {
            final int[] tmp = Arrays.copyOf(adj[i], newSize);
            toReturn[i] = tmp;
        }

        return toReturn;
    }

    private int vertexWithValue(final E e) {

        for (int i = 0; i < graph.size(); i++) {
            final Vertex<E> tmp = graph.get(i);

            if (tmp.getValue().equals(e)) {
                return i;
            }
        }

        return -1;
    }

    boolean containsValue(final E val) {
        return vertexWithValue(val) != -1;
    }

    private void priorityInsert(final int fromIndex, final int toIndex, final int distance) {
        final int indexQueue = priority.find(toIndex); // index of edge with
                                                       // same destination
                                                       // vertex, if it exists

        if (indexQueue != -1) {
            final Edge tmp = priority.get(indexQueue);
            final int prevDist = tmp.getDistance();

            if (prevDist > distance) {
                priority.removeEdge(indexQueue);

                final Edge newEdge = new Edge(fromIndex, toIndex, distance);

                priority.insert(newEdge);
            }
        } else {
            final Edge newEdge = new Edge(fromIndex, toIndex, distance);
            priority.insert(newEdge);
        }
    }

    void resetVisit() {
        for (final Vertex<E> curr : graph) {
            curr.setVisited(false);
        }
    }

}
