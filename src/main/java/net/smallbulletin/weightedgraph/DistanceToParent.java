package net.smallbulletin.weightedgraph;

public class DistanceToParent {
    private int distance;
    private int parentInd;

    public DistanceToParent(final int parentInd, final int distance) {
        this.distance = distance;
        this.parentInd = parentInd;
    }

    public int getDistance() {
        return distance;
    }

    public int getParentInd() {
        return parentInd;
    }

    public void setDistance(final int distance) {
        this.distance = distance;
    }

    public void setParentInd(final int parentInd) {
        this.parentInd = parentInd;
    }


}
