package net.smallbulletin.weightedgraph;

import java.util.ArrayList;
import java.util.List;

public class EdgeQueue {
    private final List<Edge> edges;

    public EdgeQueue() {
        edges = new ArrayList<Edge>();
    }

    public void insert(final Edge newEdge) {
        int indexToInsert;

        for (indexToInsert = 0; indexToInsert < edges.size(); indexToInsert++) {
            final Edge tmp = edges.get(indexToInsert);

            if (newEdge.getDistance() >= tmp.getDistance()) {
                break;
            }
        }

        edges.add(null);

        for (int i = edges.size() - 2; i >= indexToInsert; i--) {
            edges.set(i + 1, edges.get(i));
        }

        edges.set(indexToInsert, newEdge);

    }

    public Edge remove() {
        return edges.remove(edges.size() - 1);
    }

    public int size() {
        return edges.size();
    }

    public Edge peek() {
        return edges.get(0);
    }

    public int find(final int findIndex) {
        int index = 0;
        for (final Edge curr : edges) {
            if (curr.getToIndex() == findIndex) {
                return index;
            }

            index++;
        }

        return -1;
    }

    public Edge get(final int index) {
        return edges.get(index);
    }

    public Edge removeEdge(final int index) {
        return edges.remove(index);
    }
}
