package net.smallbulletin.stackapp;

import net.smallbulletin.stackapp.util.MeasuredStack;
import net.smallbulletin.stackapp.util.TreeNode;

public class PostfixParser implements Parser {
	private final MeasuredStack<TreeNode<Character>> stack;

	public PostfixParser(Measurements measurements) {
		this.stack = new MeasuredStack<TreeNode<Character>>(measurements);
	}

	@Override
	public TreeNode<Character> parse(String str) {
		for (int i = 0; i < str.length(); i++) {
			char c = str.charAt(i);

			onChar(c);
		}

		TreeNode<Character> tree = stack.pop();
		if (stack.isEmpty()) {
			return tree;
		}

		throw new IllegalArgumentException("Not a complete postfix string: "
				+ str);
	}

	private void onChar(char c) {
		if (Character.isWhitespace(c)) {
			return;
		}

		if (Character.isLetterOrDigit(c)) {
			TreeNode<Character> node = new TreeNode<>(c);
			stack.push(node);
		} else if (c == '+' || c == '-' || c == '*' || c == '/') {
			TreeNode<Character> right = stack.pop();
			TreeNode<Character> left = stack.pop();
			TreeNode<Character> node = new TreeNode<>(c, left, right);
			stack.push(node);
		} else {
			throw new IllegalArgumentException("Invalid character " + c);
		}
	}

}
