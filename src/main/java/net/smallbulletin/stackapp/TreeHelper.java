package net.smallbulletin.stackapp;

import net.smallbulletin.stackapp.util.TreeNode;

public class TreeHelper {

	public static String toInfix(final TreeNode<Character> node) {
		if (node == null) {
			return "";
		}
		if (node.getLeft() == null) {
			return node.getValue().toString();
		}
		return "(" + toInfix(node.getLeft()) + node.getValue()
				+ toInfix(node.getRight()) + ")";
	}
	
	public static String toPrefix(final TreeNode<Character> node) {
		if (node == null) {
			return "";
		}
		
		return node.getValue() + toPrefix(node.getLeft()) + toPrefix(node.getRight());
	}
	
	public static String toPostfix(final TreeNode<Character> node) {
		if (node == null) {
			return "";
		}
		
		if (node.getRight() == null) {
			return node.getValue().toString();
		}
		
		return toPostfix(node.getLeft()) + toPostfix(node.getRight()) + node.getValue();
	}
	
}
