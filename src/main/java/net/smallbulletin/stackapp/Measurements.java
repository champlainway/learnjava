package net.smallbulletin.stackapp;

public class Measurements {

	private int pushes = 0;
	private int pops = 0;
	private int peeks = 0;
	private int gets = 0;

	public int getPushes() {
		return pushes;
	}

	public int getPops() {
		return pops;
	}

	public int getPeeks() {
		return peeks;
	}

	public void incPushes() {
		pushes++;
	}

	public void incPops() {
		pops++;
	}

	public void incPeeks() {
		peeks++;
	}
	
	public int getGets() {
		return gets;
	}
	
	public void incGets() {
		gets++;
	}

}
