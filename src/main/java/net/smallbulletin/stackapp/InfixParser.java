package net.smallbulletin.stackapp;

import net.smallbulletin.stackapp.util.MeasuredStack;
import net.smallbulletin.stackapp.util.TreeNode;

public class InfixParser implements Parser {

    private final MeasuredStack<TreeNode<Character>> stack;

    public InfixParser(final Measurements measurements) {
        stack = new MeasuredStack<TreeNode<Character>>(measurements);
    }

    @Override
    public TreeNode<Character> parse(final String str) {
        for (int i = 0; i < str.length(); i++) {
            final char c = str.charAt(i);

            onChar(c);
        }

        final TreeNode<Character> tree = stack.pop();

        if (stack.isEmpty()) {
            return tree;
        }

        throw new IllegalArgumentException("Not a complete infix string: " + str);

    }

    private void onChar(final char c) {
        if (Character.isWhitespace(c)) {
            return;
        }

        if (Character.isLetterOrDigit(c) || isOperator(c) || c == ')') {
            final TreeNode<Character> node = new TreeNode<Character>(c);
            stack.push(node);
            while (collapseIfPossible()) {

            }
        } else if (c == '(') {
            final TreeNode<Character> node = new TreeNode<Character>(c);
            stack.push(node);
        } else {
            throw new IllegalArgumentException("Invalid character: " + c);
        }

    }

    private boolean collapseIfPossible() {
        if (stack.size() < 3) {
            return false;
        }

        final TreeNode<Character> right = stack.peek();
        final TreeNode<Character> root = stack.get(stack.size() - 2);
        final TreeNode<Character> left = stack.get(stack.size() - 3);

        if (!isComplete(root) && isComplete(left) && isComplete(right)) {

            root.setLeft(left);
            root.setRight(right);

            stack.pop();
            stack.pop();
            stack.pop();

            stack.push(root);
            return true;
        }

        if (isComplete(root) && left.getValue() == '(' && right.getValue() == ')') {
            stack.pop();
            stack.pop();
            stack.pop();

            stack.push(root);
            return true;
        }

        return false;
    }

    private static boolean isComplete(final TreeNode<Character> node) {
        if (isOperator(node)) {
            return !(node.getLeft() == null || node.getRight() == null);
        }

        return node.getValue() != '(' && node.getValue() != ')';
    }

    private static boolean isOperator(final TreeNode<Character> node) {
        final char c = node.getValue();

        return isOperator(c);
    }

    private static boolean isOperator(final char c) {
        return (c == '+' || c == '-' || c == '*' || c == '/');
    }

}
