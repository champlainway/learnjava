package net.smallbulletin.stackapp.util;

import java.util.Stack;

import net.smallbulletin.stackapp.Measurements;

public class MeasuredStack<E> {

	private final Stack<E> stack = new Stack<E>();
	private final Measurements measurements;

	public MeasuredStack(Measurements measurements) {
		this.measurements = measurements;
	}

	public boolean isEmpty() {
		return stack.isEmpty();
	}

	public E peek() {
		measurements.incPeeks();
		return stack.peek();
	}

	public E pop() {
		measurements.incPops();
		return stack.pop();
	}

	public E push(E arg0) {
		measurements.incPushes();
		return stack.push(arg0);
	}

	public Measurements getMeasurement() {
		return measurements;
	}

	public E get(int arg0) {
		measurements.incGets();
		return stack.get(arg0);
	}

	public int size() {
		return stack.size();
	}

	public E remove(int arg0) {
		measurements.incPops();
		return stack.remove(arg0);
	}

}
