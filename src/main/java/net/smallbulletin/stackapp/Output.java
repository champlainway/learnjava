package net.smallbulletin.stackapp;

import net.smallbulletin.stackapp.Measurements;

public class Output {

	private String infix;
	private String postfix;
	private String prefix;
	private Measurements measurements;
	
	public Output(String infix, Measurements measurements) {
		this.infix = infix;
		this.measurements = measurements;
	}
	
	public Output(String infix, String prefix, String postfix, Measurements measurements) {
		this.infix = infix;
		this.prefix = prefix;
		this.postfix = postfix;
		this.measurements = measurements;
	}

	public String getInfix() {
		return infix;
	}

	public void setInfix(String infix) {
		this.infix = infix;
	}

	public Measurements getMeasurements() {
		return measurements;
	}

	public void setMeasurements(Measurements measurements) {
		this.measurements = measurements;
	}

	public String getPostfix() {
		return postfix;
	}

	public void setPostfix(String postfix) {
		this.postfix = postfix;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}
	
	

}
