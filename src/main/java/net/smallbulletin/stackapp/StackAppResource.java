package net.smallbulletin.stackapp;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import net.smallbulletin.stackapp.Input;
import net.smallbulletin.stackapp.util.TreeNode;

import com.codahale.metrics.annotation.Timed;

@Path("/stackapp")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class StackAppResource {

	@POST
	@Timed
	@Path("/prefix")
	public Output parsePrefix(Input input) {
		Measurements measurements = new Measurements();

		Parser parser = new PrefixParser(measurements);

		TreeNode<Character> tree = parser.parse(input.getStr());

		String infix = TreeHelper.toInfix(tree);
		String prefix = TreeHelper.toPrefix(tree);
		String postfix = TreeHelper.toPostfix(tree);

		return new Output(infix, prefix, postfix, measurements);
	}

	@POST
	@Timed
	@Path("/infix")
	public Output parseInfix(Input input) {
		Measurements measurements = new Measurements();

		Parser parser = new InfixParser(measurements);

		TreeNode<Character> tree = parser.parse(input.getStr());

		String infix = TreeHelper.toInfix(tree);
		String prefix = TreeHelper.toPrefix(tree);
		String postfix = TreeHelper.toPostfix(tree);

		return new Output(infix, prefix, postfix, measurements);
	}

	@POST
	@Timed
	@Path("/postfix")
	public Output parsePostfix(Input input) {
		Measurements measurements = new Measurements();

		Parser parser = new PostfixParser(measurements);

		TreeNode<Character> tree = parser.parse(input.getStr());

		String infix = TreeHelper.toInfix(tree);
		String prefix = TreeHelper.toPrefix(tree);
		String postfix = TreeHelper.toPostfix(tree);

		return new Output(infix, prefix, postfix, measurements);

	}
}
