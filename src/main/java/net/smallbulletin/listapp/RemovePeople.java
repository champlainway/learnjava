package net.smallbulletin.listapp;

import java.util.ArrayList;
import java.util.List;

import net.smallbulletin.listapp.util.CircularLinkedList;

public class RemovePeople {
	private CircularLinkedList<String> names;
	private ArrayList<String> removed;
	private int skip;
	private int start;

	public RemovePeople(List<String> names, int skip, int start) {
		removed = new ArrayList<String>();
		this.names = new CircularLinkedList<>(names);
		this.skip = skip;
		this.start = start;
	}

	// TODO finish
	public String takeOut() {
		int index = 0;
		for (int i = 0; i < start; i++) {
			names.next();
		}
		while (names.size() > 1) {
			if (index < skip) {
				names.next();
				index++;
			} else {
				removed.add(names.removeCurr());
				index = 0;
			}
		}

		return names.getCurr();
	}

	public List<String> getRemoved() {
		return removed;
	}
}
