package net.smallbulletin.listapp;

import java.util.List;

public class Output {
	private List<String> origList;
	private List<String> eliminated;
	private String last;

	public Output(List<String> origList, List<String> eliminated, String last) {
		this.origList = origList;
		this.eliminated = eliminated;
		this.last = last;

	}

	public List<String> getOrigList() {
		return origList;
	}

	public String getLast() {
		return last;
	}

	public List<String> getEliminated() {
		return eliminated;
	}

}
