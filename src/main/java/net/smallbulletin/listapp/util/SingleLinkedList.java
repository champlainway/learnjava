package net.smallbulletin.listapp.util;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;

public class SingleLinkedList<E> implements List<E> {

    private final Link<E> anchor = new Link<>(null);
    private Link<E> last = anchor;
    private int size = 0;

    public SingleLinkedList() {

    }

    @Override
    public boolean add(E e) {
        Link<E> link = new Link<>(e);

        last.setNext(link);
        last = link;

        size++;
        return true;
    }

    @Override
    public void add(int index, E element) {

        Link<E> link = new Link<>(element);
        Link<E> prev = anchor;

        for (int i = 0; i < index; i++) {

            prev = prev.getNext();
            if (prev == null) {
                throw new IndexOutOfBoundsException();
            }
        }
        link.setNext(prev.getNext());
        prev.setNext(link);

        size++;

        if (link.getNext() == null) {
            last = link;
        }
    }

    @Override
    public boolean addAll(Collection<? extends E> collection) {
    	boolean changed = false;
    	
        for (E e : collection) {
        	add(e);
        	changed = true;
        }
        
        return changed;
    }

    @Override
    public boolean addAll(int index, Collection<? extends E> collection) {
        Link<E> prev = anchor;
        boolean changed = false;

        for (int i = 0; i < index; i++) {

            prev = prev.getNext();
            if (prev == null) {
                throw new IndexOutOfBoundsException();
            }
        }

        for (E e : collection) {
            Link<E> curr = new Link<>(e);

            curr.setNext(prev.getNext());
            prev.setNext(curr);
            prev = curr;

            if (curr.getNext() == null) {
                last = curr;
            }

            changed = true;
            size++;
        }

        return changed;
    }

    @Override
    public void clear() {
        anchor.setNext(null);
        last = anchor;
        size = 0;
    }

    @Override
    public boolean contains(Object obj) {
        return indexOf(obj) >= 0;
    }

    @Override
    // TODO Use a hashset to improve efficiency
    public boolean containsAll(Collection<?> collection) {

        for (Object e : collection) {
            if (!contains(e)) {
                return false;
            }
        }

        return true;
    }

    @Override
    public E get(int index) {
        Link<E> curr = linkAt(index);
        return curr.getValue();
    }

    private Link<E> linkAt(int index) {
        if (index < 0) {
            return null;
        }

        Link<E> curr = anchor.getNext();

        for (int i = 0; i < index; i++) {
            curr = curr.getNext();

            if (curr == null) {
                throw new IndexOutOfBoundsException();
            }
        }

        return curr;
    }

    @Override
    public int indexOf(Object obj) {
        Link<E> curr = anchor.getNext();
        int index = 0;

        while (curr != null) {
            E val = curr.getValue();

            if (obj == null ? val == null : obj.equals(val)) {
                return index;
            }

            curr = curr.getNext();
            index++;
        }

        return -1;
    }

    @Override
    public boolean isEmpty() {
        return last == anchor;
    }

    @Override
    public Iterator<E> iterator() {
        return new IteratorImpl();
    }

    @Override
    public int lastIndexOf(Object obj) {
        Link<E> curr = anchor.getNext();
        int index = 0;
        int last = -1;

        while (curr != null) {
            E val = curr.getValue();

            if (obj == null ? val == null : obj.equals(val)) {
                last = index;
            }

            curr = curr.getNext();
            index++;
        }

        return last;
    }

    @Override
    public ListIterator<E> listIterator() {
        return new ListIteratorImpl();
    }

    @Override
    public ListIterator<E> listIterator(int start) {

        if (start < 0 || start > size) {
            throw new IndexOutOfBoundsException();
        }

        return new ListIteratorImpl(start);
    }

    @Override
    public boolean remove(Object obj) {
        Link<E> curr = anchor;

        while (curr.getNext() != null) {
            E val = curr.getNext().getValue();

            if (obj == null ? val == null : obj.equals(val)) {
                curr.setNext(curr.getNext().getNext());
                size--;

                if (curr.getNext() == null) {
                    last = curr;
                }

                return true;
            }

            curr = curr.getNext();

        }

        return false;
    }

    @Override
    public E remove(int index) {
        Link<E> curr = anchor;

        for (int i = 0; i < index; i++) {
            curr = curr.getNext();

            if (curr == null) {
                throw new IndexOutOfBoundsException();
            }
        }
        Link<E> tmp = curr.getNext();

        curr.setNext(curr.getNext().getNext());

        size--;

        if (curr.getNext() == null) {
            last = curr;
        }

        return tmp.getValue();
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        boolean changed = false;
        Link<E> curr = anchor;

        while (curr.getNext() != null) {

            E val = curr.getNext().getValue();

            if (c.contains(val)) {
                curr.setNext(curr.getNext().getNext());
                size--;
                changed = true;

            } else {
                curr = curr.getNext();
            }
        }

        last = curr;
        return changed;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        boolean changed = false;
        Link<E> curr = anchor;

        while (curr.getNext() != null) {

            E val = curr.getNext().getValue();

            if (!c.contains(val)) {
                curr.setNext(curr.getNext().getNext());
                size--;
                changed = true;

            } else {
                curr = curr.getNext();
            }
        }

        last = curr;

        return changed;
    }

    @Override
    public E set(int index, E e) {
        Link<E> curr = anchor;

        for (int i = 0; i < index; i++) {
            curr = curr.getNext();

            if (curr.getNext() == null) {
                throw new IndexOutOfBoundsException();
            }
        }

        E oldVal = curr.getNext().getValue();
        Link<E> newNode = new Link<>(e);
        newNode.setNext(curr.getNext().getNext());
        curr.setNext(newNode);

        if (newNode.getNext() == null) {
            last = newNode;
        }
        return oldVal;

    }

    @Override
    public int size() {

        return size;
    }

    @Override
    public List<E> subList(int start, int end) {
        List<E> newList = new SingleLinkedList<>();
        Link<E> curr = anchor;
        for (int i = 0; i < size; i++) {
            curr = curr.getNext();

            if (i >= start && i < end) {
                newList.add(curr.getValue());
            }
        }

        return newList;
    }

    @Override
    public Object[] toArray() {
        Object[] array = new Object[size];
        Link<E> curr = anchor;
        int index = 0;

        while (curr.getNext() != null) {
            curr = curr.getNext();

            array[index] = curr.getValue();

            index++;
        }

        return array;
    }

    @Override
    public <T> T[] toArray(T[] arr) {
        if (arr.length <= size) {
            arr = Arrays.copyOf(arr, size);
        }

        Object[] array = arr;
        Link<E> curr = anchor;
        int index = 0;

        while (curr.getNext() != null) {
            curr = curr.getNext();
            array[index] = curr.getValue();

            index++;
        }

        if (index < array.length) {
            array[index] = null;
        }

        return arr;
    }

    class IteratorImpl implements Iterator<E> {
        private Link<E> prev = anchor;

        @Override
        public boolean hasNext() {
            return prev.getNext() != null;
        }

        @Override
        public E next() {
            prev = prev.getNext();

            return prev.getValue();
        }

    }

    class ListIteratorImpl implements ListIterator<E> {
        private Link<E> curr = anchor;
        private Link<E> prev = null;
        private int index = -1;

        public ListIteratorImpl() {

        }

        public ListIteratorImpl(int index) {
        	if (index > size || index < 0) {
        		throw new IndexOutOfBoundsException();
        	}
        	
            this.index = index - 1;
            curr = SingleLinkedList.this.linkAt(this.index);
            prev = SingleLinkedList.this.linkAt(this.index - 1);
        }

        @Override
        public void add(E e) {
        	Link<E> toAdd = new Link<>(e);
        	toAdd.setNext(curr.getNext());
            curr.setNext(toAdd);
            prev = curr;
            curr = toAdd;
            index++;
            size++;
        }

        @Override
        public boolean hasNext() {
            return nextIndex() < size;
        }

        @Override
        public boolean hasPrevious() {
            throw new UnsupportedOperationException("Method not implemented");
        }

        @Override
        public E next() {
        	if (!hasNext()) {
        		throw new NoSuchElementException();
        	}
        	
            prev = curr;
            curr = curr.getNext();
            index++;
            return curr.getValue();
        }

        @Override
        public int nextIndex() {

            return index + 1;
        }

        @Override
        public E previous() {
            throw new UnsupportedOperationException("Method not implemented");
        }

        @Override
        public int previousIndex() {
            throw new UnsupportedOperationException("Method not implemented");
        }

        @Override
        public void remove() {
            if (prev == null) {
                return;
            }

            prev.setNext(curr.getNext());
            curr = prev;
            if (curr.getNext() == null) {
                last = curr;
            }
            prev = null;
            size--;
            index--;
        }

        @Override
        public void set(E e) {
            Link<E> newLink = new Link<E>(e);

            prev.setNext(newLink);
            newLink.setNext(curr.getNext());
            curr = newLink;
            if (newLink.getNext() == null) {
                last = newLink;
            }
        }

    }
}
