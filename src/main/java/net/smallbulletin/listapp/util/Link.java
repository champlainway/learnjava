package net.smallbulletin.listapp.util;

class Link<E> {
    private Link<E> next;
    private final E value;

    public Link(E value) {
        this.value = value;
    }

    public Link(E value, Link<E> next) {
        this.value = value;
        this.next = next;
       
    }

    public Link<E> getNext() {
        return next;
    }

    public E getValue() {
        return value;
    }

    public void setNext(Link<E> next) {
        this.next = next;
    }

   

}
