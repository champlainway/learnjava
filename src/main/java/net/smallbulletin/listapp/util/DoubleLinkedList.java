package net.smallbulletin.listapp.util;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;

public class DoubleLinkedList<E> implements List<E> {

	private final DoubleLink<E> anchor = new DoubleLink<>(null);
	private int size = 0;

	public DoubleLinkedList() {
		anchor.setNext(anchor);
		anchor.setPrev(anchor);
	}

	@Override
	public boolean add(E e) {
		DoubleLink<E> newLink = new DoubleLink<>(e);
		DoubleLink<E> last = anchor.getPrev();
		last.setNext(newLink);
		newLink.setPrev(last);
		newLink.setNext(anchor);
		anchor.setPrev(newLink);
		size++;
		return true;
	}

	@Override
	public void add(int index, E element) {
		DoubleLink<E> newLink = new DoubleLink<>(element);
		DoubleLink<E> curr = anchor;
		int currIndex = 0;

		while (curr.getNext() != anchor && currIndex < index) {
			curr = curr.getNext();
			currIndex++;

		}

		if (index > currIndex) {
			throw new IndexOutOfBoundsException();
		}

		curr.getNext().setPrev(newLink);
		newLink.setNext(curr.getNext());
		curr.setNext(newLink);
		newLink.setPrev(curr);

		size++;
	}

	@Override
	public boolean addAll(Collection<? extends E> c) {
		boolean changed = false;

		for (E e : c) {
			add(e);
			changed = true;
		}

		return changed;
	}

	@Override
	public boolean addAll(int index, Collection<? extends E> c) {
		DoubleLink<E> curr = linkAt(index - 1);
		boolean changed = false;

		for (E e : c) {

			DoubleLink<E> newLink = new DoubleLink<>(e);
			curr.getNext().setPrev(newLink);
			newLink.setNext(curr.getNext());
			curr.setNext(newLink);
			newLink.setPrev(curr);
			curr = curr.getNext();

			changed = true;
			size++;
		}

		return changed;
	}

	@Override
	public void clear() {
		anchor.setNext(anchor);
		anchor.setPrev(anchor);
		size = 0;
	}

	@Override
	public boolean contains(Object o) {
		return indexOf(o) >= 0;
	}

	@Override
	public boolean containsAll(Collection<?> c) {

		for (Object obj : c) {
			if (!contains(obj)) {
				return false;
			}
		}

		return true;
	}

	@Override
	public E get(int index) {
		return linkAt(index).getValue();
	}

	private DoubleLink<E> linkAt(int index) {
		DoubleLink<E> curr = anchor.getNext();
		int currIndex = 0;

		while (curr != anchor && currIndex < index) {
			curr = curr.getNext();
			currIndex++;
		}

		if (index > currIndex) {
			throw new IndexOutOfBoundsException();
		}

		return curr;
	}

	@Override
	public int indexOf(Object obj) {
		DoubleLink<E> curr = anchor.getNext();
		int currIndex = 0;

		while (curr != anchor) {
			E val = curr.getValue();

			if (obj == null ? val == null : obj.equals(val)) {
				return currIndex;
			}

			curr = curr.getNext();
			currIndex++;

		}

		return -1;
	}

	@Override
	public boolean isEmpty() {
		return anchor.getNext() == anchor && anchor.getPrev() == anchor;
	}

	@Override
	public Iterator<E> iterator() {
		return new IteratorImpl();
	}

	@Override
	public int lastIndexOf(Object obj) {
		DoubleLink<E> curr = anchor.getPrev();
		int index = size - 1;

		while (curr != anchor) {
			Object val = curr.getValue();
			if (obj == null ? val == null : obj.equals(val)) {
				return index;
			}

			index--;
			curr = curr.getPrev();
		}

		return -1;
	}

	@Override
	public ListIterator<E> listIterator() {
		return new ListIteratorImpl();
	}

	@Override
	public ListIterator<E> listIterator(int index) {
		return new ListIteratorImpl(index);
	}

	@Override
	public boolean remove(Object obj) {
		DoubleLink<E> curr = anchor.getNext();

		while (curr != anchor) {
			E val = curr.getValue();

			if (obj == null ? val == null : obj.equals(val)) {
				curr.getPrev().setNext(curr.getNext());
				curr.getNext().setPrev(curr.getPrev());
				size--;
				return true;
			}

			curr = curr.getNext();
		}

		return false;
	}

	@Override
	public E remove(int index) {
		DoubleLink<E> curr = linkAt(index);

		E tmp = curr.getValue();

		curr.getPrev().setNext(curr.getNext());
		curr.getNext().setPrev(curr.getPrev());
		size--;

		return tmp;
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		boolean changed = false;
		DoubleLink<E> curr = anchor;

		while (curr.getNext() != anchor) {
			E val = curr.getNext().getValue();

			if (c.contains(val)) {
				curr.setNext(curr.getNext().getNext());
				curr.getNext().getNext().setPrev(curr);
				size--;
				changed = true;
			} else {
				curr = curr.getNext();
			}
		}

		return changed;
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		boolean changed = false;
		DoubleLink<E> curr = anchor;

		while (curr.getNext() != anchor) {
			E val = curr.getNext().getValue();

			if (!c.contains(val)) {
				curr.setNext(curr.getNext().getNext());
				curr.getNext().getNext().setPrev(curr);
				size--;
				changed = true;
			} else {
				curr = curr.getNext();
			}
		}

		return changed;
	}

	@Override
	public E set(int index, E element) {
		DoubleLink<E> curr = linkAt(index);
		DoubleLink<E> newLink = new DoubleLink<E>(element);

		newLink.setPrev(curr.getPrev());
		newLink.setNext(curr.getNext());
		curr.getPrev().setNext(newLink);
		curr.getNext().setPrev(newLink);

		return curr.getValue();
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public List<E> subList(int fromIndex, int toIndex) {
		DoubleLinkedList<E> subList = new DoubleLinkedList<>();

		DoubleLink<E> curr = anchor;

		for (int i = 0; i < size; i++) {
			curr = curr.getNext();

			if (i >= fromIndex && i < toIndex) {
				subList.add(curr.getValue());
			}
		}

		return subList;
	}

	@Override
	public Object[] toArray() {
		Object[] array = new Object[size];
		DoubleLink<E> curr = anchor;
		int index = 0;

		while (curr.getNext() != anchor) {
			curr = curr.getNext();

			array[index] = curr.getValue();

			index++;
		}

		return array;
	}

	@Override
	public <T> T[] toArray(T[] a) {
		if (a.length <= size) {
			a = Arrays.copyOf(a, size);
		}

		Object[] array = a;
		DoubleLink<E> curr = anchor;
		int index = 0;

		while (curr.getNext() != anchor) {
			curr = curr.getNext();
			array[index] = curr.getValue();

			index++;
		}

		if (index < array.length) {
			array[index] = null;
		}

		return a;
	}

	public String toString() {
		String str = "[";
		DoubleLink<E> curr = anchor;
		while (curr.getNext() != anchor) {
			curr = curr.getNext();
			str += curr.getValue();
			if (curr.getNext() != anchor) {
				str += ", ";
			}

		}
		str += "]";
		return str;
	}

	class IteratorImpl implements Iterator<E> {
		private DoubleLink<E> curr = anchor;

		@Override
		public boolean hasNext() {
			return curr.getNext() != anchor;
		}

		@Override
		public E next() {
			if (!hasNext()) {
				throw new NoSuchElementException();
			}

			curr = curr.getNext();

			return curr.getValue();
		}

	}

	class ListIteratorImpl implements ListIterator<E> {
		private int index = -1;
		private DoubleLink<E> prev = anchor;
		private boolean canRemovePrev = false;
		private boolean canRemoveNext = false;

		public ListIteratorImpl() {

		}

		public ListIteratorImpl(int index) {
			if (index > size || index < 0) {
				throw new IndexOutOfBoundsException();
			}
			this.index = index - 1;
			prev = DoubleLinkedList.this.linkAt(previousIndex());
		}

		@Override
		public void add(E e) {
			DoubleLink<E> add = new DoubleLink<>(e);

			prev.getNext().setPrev(add);
			add.setNext(prev.getNext());
			prev.setNext(add);
			add.setPrev(prev);
			index++;
			prev = add;
			size++;
		}

		@Override
		public boolean hasNext() {
			return nextIndex() < size;
		}

		@Override
		public boolean hasPrevious() {
			return previousIndex() >= 0;
		}

		@Override
		public E next() {
			if (!hasNext()) {
				throw new NoSuchElementException();
			}

			index++;
			canRemoveNext = true;
			canRemovePrev = false;
			prev = prev.getNext();
			return prev.getValue();
		}

		@Override
		public int nextIndex() {

			return index + 1;
		}

		@Override
		public E previous() {
			if (!hasPrevious()) {
				throw new NoSuchElementException();
			}

			canRemovePrev = true;
			canRemoveNext = false;
			E val = prev.getValue();
			prev = prev.getPrev();
			index--;
			return val;
		}

		@Override
		public int previousIndex() {
			return index;
		}

		@Override
		public void remove() {
			if (!canRemovePrev && !canRemoveNext) {
				throw new IllegalStateException("Can't remove again");
			}

			if (canRemovePrev) {
				remove(prev.getNext());
				canRemovePrev = false;
				size--;
			} else if (canRemoveNext) {
				remove(prev);
				index--;
				canRemoveNext = false;
				size--;
			}
		}

		@Override
		public void set(E e) {

			DoubleLink<E> link = new DoubleLink<>(e);

			link.setNext(prev.getNext());
			link.setPrev(prev.getPrev());
			prev.getPrev().setNext(link);
			prev.getNext().setPrev(link);
			prev = link;

		}

		private void remove(DoubleLink<E> toRemove) {
			toRemove.getPrev().setNext(toRemove.getNext());
			toRemove.getNext().setPrev(toRemove.getPrev());
		}

	}

}
