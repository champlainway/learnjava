package net.smallbulletin.sortapp.util;

import java.util.ArrayList;
import java.util.Collection;

import com.google.common.collect.ImmutableList;

import net.smallbulletin.sortapp.Measurements;

public class MeasuredArray<E extends Comparable<E>> {

    private ArrayList<E> array;
    private Measurements measurements;

    public MeasuredArray() {

    }

    public MeasuredArray(final String algorithm) {

        array = new ArrayList<>();
        measurements = new Measurements(algorithm);
    }

    public MeasuredArray(final String algorithm, final Collection<E> base) {
        this(algorithm);
        array.addAll(base);
    }

    public boolean add(final E arg0) {
        return array.add(arg0);
    }

    public void add(final int arg0, final E arg1) {
        array.add(arg0, arg1);
    }

    public E get(final int arg0) {
        measurements.incGets();
        return array.get(arg0);
    }

    public boolean isEmpty() {
        return array.isEmpty();
    }

    public E set(final int arg0, final E arg1) {
        measurements.incSets();
        return array.set(arg0, arg1);
    }

    public int size() {
        return array.size();
    }

    public int compare(final int i, final int j) {
        measurements.incCompares();

        return get(i).compareTo(get(j));
    }

    public int compare(final int i, final E j) {
        measurements.incCompares();

        return get(i).compareTo(j);
    }

    public ImmutableList<E> asImmutableList() {
        return ImmutableList.copyOf(array);
    }

    public Measurements getMeasurements() {
        return measurements;
    }

    public MeasuredArray<E> subList(final int i) {
        final MeasuredArray<E> sub = new MeasuredArray<>();
        for (int j = i; j < size(); j++) {
            sub.add(get(j));
        }

        return sub;
    }

    public MeasuredArray<E> subList(final int i, final int j) {
        final MeasuredArray<E> sub = new MeasuredArray<>();

        for (int k = i; k < j; k++) {
            sub.add(get(k));
        }

        return sub;
    }

    public E remove(final int i) {
        return array.remove(i);
    }

}
