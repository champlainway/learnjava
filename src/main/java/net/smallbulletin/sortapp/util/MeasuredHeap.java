package net.smallbulletin.sortapp.util;

import net.smallbulletin.sortapp.Measurements;

public class MeasuredHeap<E extends Comparable<E>> {

    private final MeasuredArray<E> heap;
    private int size = 0;

    public MeasuredHeap(final MeasuredArray<E> heap) {
        this.heap = heap;
        size = 0;
    }

    public E remove() {

        final E toReturn = heap.get(0);

        if (size == 1) {
            heap.remove(0);
            size--;
            return toReturn;
        }

        heap.set(0, heap.remove(size - 1));
        size--;
        int currIndex = 0;
        int leftChildIndex = 1;
        int rightChildIndex = 2;
        E toSwap;
        int swapIndex;

        if (leftChildIndex >= size && rightChildIndex >= size) {
            return toReturn;
        } else if (rightChildIndex >= size
                || (leftChildIndex < size && heap.compare(leftChildIndex, rightChildIndex) < 0)) {
            toSwap = heap.get(leftChildIndex);
            swapIndex = leftChildIndex;
        } else if (leftChildIndex >= size
                || (rightChildIndex < size && heap.compare(leftChildIndex, rightChildIndex) > 0)) {
            toSwap = heap.get(rightChildIndex);
            swapIndex = rightChildIndex;
        } else {
            return toReturn;
        }

        while (heap.compare(currIndex, toSwap) > 0) {

            swap(currIndex, swapIndex);

            currIndex = swapIndex;
            leftChildIndex = swapIndex * 2 + 1;
            rightChildIndex = swapIndex * 2 + 2;

            if (leftChildIndex >= size && rightChildIndex >= size) {
                return toReturn;
            } else if (rightChildIndex >= size
                    || (leftChildIndex < size && heap.compare(leftChildIndex, rightChildIndex) < 0)) {
                toSwap = heap.get(leftChildIndex);
                swapIndex = leftChildIndex;
            } else if (leftChildIndex >= size
                    || (rightChildIndex < size && heap.compare(leftChildIndex, rightChildIndex) > 0)) {
                toSwap = heap.get(rightChildIndex);
                swapIndex = rightChildIndex;
            } else {
                return toReturn;
            }
        }

        return toReturn;
    }

    public void insert(final E val) {
        size++;
        heap.add(val);

        int index = size - 1;
        int parentIndex = (index - 1) / 2;

        while (heap.compare(index, parentIndex) < 0) {
            swap(index, parentIndex);
            index = parentIndex;
            parentIndex = (index - 1) / 2;
        }
    }

    private void swap(final int i, final int j) {
        final E tmp = heap.get(i);
        heap.set(i, heap.get(j));
        heap.set(j, tmp);
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public Measurements getMeasurements() {
        return heap.getMeasurements();
    }
}
