package net.smallbulletin.sortapp;

import com.google.common.collect.ImmutableList;

public class Input<E extends Comparable<E>> {

	private ImmutableList<E> unsorted;

	public ImmutableList<E> getUnsorted() {
		return unsorted;
	}

	public void setUnsorted(ImmutableList<E> unsorted) {
		this.unsorted = unsorted;
	}

}
