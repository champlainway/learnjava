package net.smallbulletin.sortapp;

import net.smallbulletin.sortapp.impl.BubbleSort;
import net.smallbulletin.sortapp.impl.HeapSort;
import net.smallbulletin.sortapp.impl.InsertionSort;
import net.smallbulletin.sortapp.impl.MergeSort;
import net.smallbulletin.sortapp.impl.QuickSort;
import net.smallbulletin.sortapp.impl.SelectionSort;
import net.smallbulletin.sortapp.impl.ShellSort;

public class SortAlgorithmFactory {

    private SortAlgorithmFactory() {

    }

    public static <E extends Comparable<E>> SortAlgorithm<E> of(final SortAlgorithmName name) {
        switch (name) {

        case SELECTION_SORT:
            return new SelectionSort<E>();
        case INSERTION_SORT:
            return new InsertionSort<E>();
        case MERGE_SORT:
            return new MergeSort<E>();
        case SHELL_SORT:
            return new ShellSort<E>();
        case QUICK_SORT:
            return new QuickSort<E>();
        case HEAP_SORT:
            return new HeapSort<E>();
        case BUBBLE_SORT:
            return new BubbleSort<E>();
        default:
            return new BubbleSort<E>();

        }
    }
}
