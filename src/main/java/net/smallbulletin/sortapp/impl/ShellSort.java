package net.smallbulletin.sortapp.impl;

import java.util.List;

import net.smallbulletin.sortapp.Output;
import net.smallbulletin.sortapp.SortAlgorithm;
import net.smallbulletin.sortapp.SortAlgorithmName;
import net.smallbulletin.sortapp.util.MeasuredArray;

public class ShellSort<E extends Comparable<E>> implements SortAlgorithm<E> {

    @Override
    public Output<E> sort(final List<E> input) {
        final MeasuredArray<E> sorted = new MeasuredArray<>(SortAlgorithmName.SHELL_SORT.name(), input);

        int h = 1;
        while (h <= sorted.size() / 3) {
            h = h * 3 + 1;
        }

        while (h > 0) {
            for (int i = h; i < sorted.size(); i++) {

                final E tmp = sorted.get(i);
                int j = i;

                while (j > h - 1 && sorted.compare(j - h, tmp) > 0) {
                    sorted.set(j, sorted.get(j - h));
                    j -= h;
                }

                sorted.set(j, tmp);
            }

            h = (h - 1) / 3;
        }

        final Output<E> output = new Output<>(sorted.asImmutableList(), sorted.getMeasurements());

        return output;

    }

}
