package net.smallbulletin.sortapp.impl;

import java.util.List;

import net.smallbulletin.sortapp.Output;
import net.smallbulletin.sortapp.SortAlgorithm;
import net.smallbulletin.sortapp.SortAlgorithmName;
import net.smallbulletin.sortapp.util.MeasuredArray;

public class MergeSort<E extends Comparable<E>> implements SortAlgorithm<E> {

    @Override
    public Output<E> sort(final List<E> input) {

        final MeasuredArray<E> sorted = new MeasuredArray<>(SortAlgorithmName.MERGE_SORT.name(), input);
        final MeasuredArray<E> tmp = new MeasuredArray<>(SortAlgorithmName.MERGE_SORT.name(), input);
        mergeSort(sorted, tmp, 0, sorted.size() - 1);

        final Output<E> output = new Output<>(sorted.asImmutableList(), sorted.getMeasurements());

        return output;
    }

    private void mergeSort(final MeasuredArray<E> realArray, final MeasuredArray<E> tmpArray, final int lower,
            final int upper) {
        if (lower == upper) {
            return;
        }

        final int mid = (lower + upper) / 2;

        mergeSort(realArray, tmpArray, lower, mid);
        mergeSort(realArray, tmpArray, mid + 1, upper);
        merge(realArray, tmpArray, lower, mid + 1, upper);
    }

    private void merge(final MeasuredArray<E> realArray, final MeasuredArray<E> tmpArray, int startLower,
            int startUpper, final int endUpper) {
        int i = 0;
        final int low = startLower;
        final int mid = startUpper - 1;
        final int length = endUpper - startLower + 1;

        while (startLower <= mid && startUpper <= endUpper) {
            if (realArray.compare(startLower, startUpper) < 0) {
                tmpArray.set(i, realArray.get(startLower));
                i++;
                startLower++;
            } else {
                tmpArray.set(i, realArray.get(startUpper));
                i++;
                startUpper++;
            }
        }

        while (startLower <= mid) {
            tmpArray.set(i, realArray.get(startLower));
            i++;
            startLower++;
        }

        while (startUpper <= endUpper) {
            tmpArray.set(i, realArray.get(startUpper));
            i++;
            startUpper++;
        }

        for (i = 0; i < length; i++) {
            realArray.set(low + i, tmpArray.get(i));
        }
    }

}
