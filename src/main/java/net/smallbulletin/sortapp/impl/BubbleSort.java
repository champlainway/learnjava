package net.smallbulletin.sortapp.impl;

import java.util.List;

import net.smallbulletin.sortapp.Output;
import net.smallbulletin.sortapp.SortAlgorithm;
import net.smallbulletin.sortapp.SortAlgorithmName;
import net.smallbulletin.sortapp.util.MeasuredArray;

public class BubbleSort<E extends Comparable<E>> implements SortAlgorithm<E> {

    @Override
    public Output<E> sort(final List<E> input) {
        final MeasuredArray<E> sorted = new MeasuredArray<>(SortAlgorithmName.BUBBLE_SORT.name(), input);

        for (int i = sorted.size() - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                // E curr = sorted.get(j);
                // E next = sorted.get(j + 1);
                if (sorted.compare(j, j + 1) > 0) {
                    final E tmp = sorted.get(j);
                    sorted.set(j, sorted.get(j + 1));
                    sorted.set(j + 1, tmp);
                }
            }
        }

        final Output<E> output = new Output<>(sorted.asImmutableList(), sorted.getMeasurements());

        return output;
    }
}
