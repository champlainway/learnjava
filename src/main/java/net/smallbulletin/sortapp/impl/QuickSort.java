package net.smallbulletin.sortapp.impl;

import java.util.List;

import net.smallbulletin.sortapp.Output;
import net.smallbulletin.sortapp.SortAlgorithm;
import net.smallbulletin.sortapp.SortAlgorithmName;
import net.smallbulletin.sortapp.util.MeasuredArray;

public class QuickSort<E extends Comparable<E>> implements SortAlgorithm<E> {

    @Override
    public Output<E> sort(final List<E> input) {

        final MeasuredArray<E> sorted = new MeasuredArray<>(SortAlgorithmName.QUICK_SORT.name(), input);

        quickSort(sorted, 0, sorted.size() - 1);

        final Output<E> output = new Output<>(sorted.asImmutableList(), sorted.getMeasurements());

        return output;
    }

    private <E extends Comparable<E>> void quickSort(final MeasuredArray<E> input, final int left, final int right) {
        if (right - left <= 0) {
            return;

        } else if (input.size() <= 3) {
            simpleSort(input);
        } else {

            final int part = partition(input, left, right);
            quickSort(input, left, part - 1);
            quickSort(input, part + 1, right);
        }
    }

    private <E extends Comparable<E>> int partition(final MeasuredArray<E> input, final int left, final int right) {
        int leftScan = left - 1;
        int rightScan = right;

        final E pivot = median(input, left, right);

        while (true) {
            while (left < right && input.compare(++leftScan, pivot) < 0) {

            }

            while (rightScan > 0 && input.compare(--rightScan, pivot) > 0) {

            }

            if (leftScan >= rightScan) {
                break;
            } else {
                swap(input, leftScan, rightScan);
            }
        }
        swap(input, leftScan, right);
        return leftScan;
    }

    private <E extends Comparable<E>> void swap(final MeasuredArray<E> input, final int i, final int j) {
        if (i == j) {
            return;
        }
        final E tmp = input.get(i);
        input.set(i, input.get(j));
        input.set(j, tmp);
    }

    private <E extends Comparable<E>> E median(final MeasuredArray<E> input, final int left, final int right) {
        final int mid = (left + right) / 2;

        if (input.compare(left, mid) > 0) {
            swap(input, left, mid);
        }

        if (input.compare(left, right) > 0) {
            swap(input, left, right);

        }

        if (input.compare(mid, right) > 0) {
            swap(input, mid, right);
        }

        swap(input, mid, right);

        return input.get(right);
    }

    private <E extends Comparable<E>> void simpleSort(final MeasuredArray<E> input) {
        if (input.size() == 1) {
            return;
        }

        if (input.size() == 2) {
            if (input.compare(0, 1) > 0) {
                swap(input, 0, 1);
            }
        } else if (input.size() == 3) {
            if (input.compare(0, 1) > 0) {
                swap(input, 0, 1);
            }

            if (input.compare(0, 2) > 0) {
                swap(input, 0, 2);
            }

            if (input.compare(1, 2) > 0) {
                swap(input, 1, 2);
            }
        }
    }

}
