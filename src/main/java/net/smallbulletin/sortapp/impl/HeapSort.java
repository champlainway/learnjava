package net.smallbulletin.sortapp.impl;

import java.util.List;

import net.smallbulletin.sortapp.Output;
import net.smallbulletin.sortapp.SortAlgorithm;
import net.smallbulletin.sortapp.util.MeasuredArray;
import net.smallbulletin.sortapp.util.MeasuredHeap;

public class HeapSort<E extends Comparable<E>> implements SortAlgorithm<E> {

    @Override
    public Output<E> sort(final List<E> input) {

        final MeasuredArray<E> sorted = new MeasuredArray<E>("HEAP_SORT");

        final MeasuredHeap<E> tmp = new MeasuredHeap<>(sorted);

        for (final E e : input) {
            tmp.insert(e);
        }

        while (!tmp.isEmpty()) {
            final E e = tmp.remove();
            sorted.add(e);
        }

        final Output<E> output = new Output<E>(sorted.asImmutableList(), sorted.getMeasurements());

        return output;
    }

}
