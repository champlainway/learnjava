package net.smallbulletin.sortapp.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.google.common.collect.Lists;

import net.smallbulletin.sortapp.Output;
import net.smallbulletin.sortapp.SortAlgorithm;
import net.smallbulletin.sortapp.SortAlgorithmName;
import net.smallbulletin.sortapp.util.MeasuredArray;

public class RadixSort implements SortAlgorithm<Integer> {

    @Override
    public Output<Integer> sort(final List<Integer> input) {
        final MeasuredArray<Integer> toSort = new MeasuredArray<>(SortAlgorithmName.RADIX_SORT.name(), input);
        Integer m = toSort.get(0);
        int exp = 1;
        final int size = toSort.size();
        final Integer[] arr = new Integer[10];
        final List<Integer> sorted = new ArrayList<Integer>(Arrays.asList(arr));

        int i = 1;

        for (i = 1; i < size; i++) {
            if (toSort.get(i).compareTo(m) > 0) {
                m = toSort.get(i);
            }
        }

        while (m / exp > 0) {
            final Integer[] array = new Integer[10];
            final List<Integer> sameDigit = new ArrayList<Integer>(Arrays.asList(array));

            for (i = 0; i < size; i++) {
                final int index = (toSort.get(i) / exp) % 10;
                final Integer curr = sameDigit.get(index);
                if (curr == null) {
                    sameDigit.set(index, 1);
                } else {
                    sameDigit.set(index, sameDigit.get(index) + 1);
                }

            }

            for (i = 1; i < 10; i++) {
                final Integer curr = sameDigit.get(i);
                final Integer toAdd = sameDigit.get(i - 1);
                if (curr == null) {
                    sameDigit.set(i, toAdd);
                } else {
                    sameDigit.set(i, curr + toAdd);
                }
            }

            for (i = size - 1; i >= 0; i--) {
                final int a = (toSort.get(i) / exp) % 10;
                final int index = sameDigit.get(a % 10) - 1;
                sameDigit.set(a, index);
                sorted.set(index, toSort.get(i));
            }

            for (i = 0; i < size; i++) {
                Integer toSet = sorted.get(i);

                if (toSet == null) {
                    toSet = 0;
                }
                toSort.set(i, toSet);
            }

            exp *= 10;
        }

        final Output<Integer> output = new Output<>(toSort.asImmutableList(), toSort.getMeasurements());

        return output;
    }

}
