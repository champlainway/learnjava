package net.smallbulletin.sortapp;

public class Measurements {

    private final String sort;
    private int gets = 0;
    private int sets = 0;
    private int compares = 0;

    public Measurements(final String sort) {
        super();
        this.sort = sort;
    }

    public void incGets() {
        gets++;
    }

    public void incSets() {
        sets++;
    }

    public void incCompares() {
        compares++;
    }

    public int getGets() {
        return gets;
    }

    public int getSets() {
        return sets;
    }

    public int getCompares() {
        return compares;
    }

    public String getSort() {
        return sort;
    }

    private void setGets(final int gets) {
        this.gets = gets;
    }

    private void setCompares(final int compares) {
        this.compares = compares;
    }

    private void setSets(final int sets) {
        this.sets = sets;
    }

    public Measurements combine(final Measurements other) {
        final Measurements newMeasure = new Measurements(sort);
        newMeasure.setCompares(compares + other.getCompares());
        newMeasure.setGets(gets + other.getGets());
        newMeasure.setSets(sets + other.getSets());

        return newMeasure;
    }

}
