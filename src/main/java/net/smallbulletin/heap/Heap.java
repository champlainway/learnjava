package net.smallbulletin.heap;

public interface Heap<E extends Comparable<E>> {
    E remove();

    void insert(E val);

}
