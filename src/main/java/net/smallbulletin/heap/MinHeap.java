package net.smallbulletin.heap;

import java.util.List;

import com.google.common.collect.Lists;

public class MinHeap<E extends Comparable<E>> implements Heap<E> {

    private final List<E> heap;

    public MinHeap() {
        heap = Lists.newArrayList();
    }

    @Override
    public E remove() {
        final E toReturn = heap.get(0);

        if (heap.size() == 1) {
            heap.remove(0);
            return toReturn;
        }

        heap.set(0, heap.remove(heap.size() - 1));
        int currIndex = 0;
        int leftChildIndex = 1;
        int rightChildIndex = 2;
        E toSwap;
        int swapIndex;

        if (leftChildIndex >= heap.size() && rightChildIndex >= heap.size()) {
            return toReturn;
        } else if (rightChildIndex >= heap.size() || (leftChildIndex < heap.size()
                && heap.get(leftChildIndex).compareTo(heap.get(rightChildIndex)) < 0)) {
            toSwap = heap.get(leftChildIndex);
            swapIndex = leftChildIndex;
        } else if (leftChildIndex >= heap.size() || (rightChildIndex < heap.size()
                && heap.get(leftChildIndex).compareTo(heap.get(rightChildIndex)) > 0)) {
            toSwap = heap.get(rightChildIndex);
            swapIndex = rightChildIndex;
        } else {
            return toReturn;
        }

        while (heap.get(currIndex).compareTo(toSwap) > 0) {

            swap(currIndex, swapIndex);

            currIndex = swapIndex;
            leftChildIndex = swapIndex * 2 + 1;
            rightChildIndex = swapIndex * 2 + 2;

            if (leftChildIndex >= heap.size() && rightChildIndex >= heap.size()) {
                return toReturn;
            } else if (rightChildIndex >= heap.size() || (leftChildIndex < heap.size()
                    && heap.get(leftChildIndex).compareTo(heap.get(rightChildIndex)) < 0)) {
                toSwap = heap.get(leftChildIndex);
                swapIndex = leftChildIndex;
            } else if (leftChildIndex >= heap.size() || (rightChildIndex < heap.size()
                    && heap.get(leftChildIndex).compareTo(heap.get(rightChildIndex)) > 0)) {
                toSwap = heap.get(rightChildIndex);
                swapIndex = rightChildIndex;
            } else {
                return toReturn;
            }
        }

        return toReturn;
    }

    @Override
    public void insert(final E val) {
        heap.add(val);

        int index = heap.size() - 1;
        int parentIndex = (index - 1) / 2;

        while (heap.get(index).compareTo(heap.get(parentIndex)) < 0) {
            swap(index, parentIndex);
            index = parentIndex;
            parentIndex = (index - 1) / 2;
        }
    }

    private void swap(final int i, final int j) {
        final E tmp = heap.get(i);
        heap.set(i, heap.get(j));
        heap.set(j, tmp);
    }
}
