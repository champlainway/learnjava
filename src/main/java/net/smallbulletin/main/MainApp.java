package net.smallbulletin.main;

import net.smallbulletin.huffman.HuffmanAppResource;
import net.smallbulletin.listapp.ListAppResource;
import net.smallbulletin.recursionapp.RecursionAppResource;
import net.smallbulletin.sortapp.SortAppResource;
import net.smallbulletin.stackapp.StackAppResource;
import io.dropwizard.Application;
import io.dropwizard.setup.Environment;

public class MainApp extends Application<MainAppConfiguration> {

    public static void main(final String[] args) throws Exception {

        new MainApp().run(args);
    }

    @Override
    public void run(final MainAppConfiguration config, final Environment env) throws Exception {
        final SortAppResource sortapp = new SortAppResource();
        env.jersey().register(sortapp);
        final StackAppResource stackapp = new StackAppResource();
        env.jersey().register(stackapp);
        final ListAppResource listapp = new ListAppResource();
        env.jersey().register(listapp);
        final RecursionAppResource recurseApp = new RecursionAppResource();
        env.jersey().register(recurseApp);
        final HuffmanAppResource huffmanApp = new HuffmanAppResource();
        env.jersey().register(huffmanApp);
    }

}
